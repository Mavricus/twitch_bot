import { IAppConfig } from '../configuration/corfig_reader';
import tmi, { Client } from 'tmi.js';

export interface IClientFactory {
    create(): Client;
}

export class ClientFactory implements IClientFactory {
    constructor(private config: IAppConfig) {
    }

    create(): Client {
        return tmi.client(this.config.bot.config);
    }
}