import { IDataStorage, ITypedDataStorageFactory } from '../../utils/storages/storage_types';
import { ILogger } from '../../utils/logger/base_logger';
import { IUserDbStorage } from './user_db_storage';
import { IUserDto } from '../../data_sources/users/user_dao';
import { IUserState } from '../bot_event_processors/user_state_builder';
import { IChatUser, IViolationPenalty } from '../bot_event_processors/chat_message_event_processor/chat_message_handlers/moderation/moderate_types';

export interface IChatUserStorage {
    get(requestId: string, user: IUserState): Promise<IChatUser>;
    getByName(requestId: string, userName: string): Promise<IChatUser | null>;
    getById(requestId: string, userId: string): Promise<IChatUser | null>;
}

export class ChatUserStorage implements IChatUserStorage {
    constructor(private storage: IDataStorage,
                private userPersistentInfo: IUserDbStorage,
                private penaltyStorageFactory: ITypedDataStorageFactory<IViolationPenalty>,
                private logger: ILogger) {
    }

    async getByName(requestId: string, userName: string): Promise<IChatUser | null> {
        const userInfo = await this.userPersistentInfo.getByTwitchName(requestId, userName);
        if (userInfo == null) {
            return null;
        }

        const storageKey = ChatUserStorage.buildKey(userInfo.twitch_id);
        const user = await this.storage.get<IChatUser>(storageKey);

        if (user != null) {
            return user;
        }
        
        const data: IChatUser = this.createUserInfo(userInfo);
        await this.storage.set<IChatUser>(storageKey, data);
        return data;
    }

    async getById(requestId: string, userId: string): Promise<IChatUser | null> {
        const storageKey = ChatUserStorage.buildKey(userId);
        const user = await this.storage.get<IChatUser>(storageKey);

        if (user != null) {
            return user;
        }

        const userInfo = await this.userPersistentInfo.getByTwitchId(requestId, userId);
        if (userInfo == null) {
            return null;
        }
        const data: IChatUser = this.createUserInfo(userInfo);
        await this.storage.set<IChatUser>(storageKey, data);
        return data;
    }

    async get(requestId: string, user: IUserState): Promise<IChatUser> {
        const storageKey = ChatUserStorage.buildKey(user.id);
        let userInfo = await this.storage.get<IChatUser>(storageKey);

        if (userInfo != null) {
            this.logger.trace({ description: 'Returning existed user', requestId, userInfo });
            return userInfo;
        }
        
        let userPersistInfo = await this.userPersistentInfo.getByTwitchId(requestId, user.id);
        if (userPersistInfo == null) {
            userPersistInfo = await this.userPersistentInfo.create(requestId, { name: user.name, twitch_id: user.id });
        }
        if (userPersistInfo.name !== user.name) {
            userPersistInfo = await this.userPersistentInfo.update(requestId,{ id: userPersistInfo.id, name: user.name, twitch_id: user.id });
        }

        userInfo = this.createUserInfo(userPersistInfo);
        await this.storage.set<IChatUser>(storageKey, userInfo);
        this.logger.trace( { description: 'Created new user', requestId, userInfo: userInfo });
        return userInfo;
    }

    private static buildKey(twitchId: string) {
        if (twitchId == null) {
            throw new Error('User does not have an ID');
        }
        return 'user:' + twitchId;
    }

    private createUserInfo(user: IUserDto): IChatUser {
        if (user.id == null) {
            throw new Error('User ID cannot be a null');
        }

        return {
            id: user.id,
            name: user.name ?? '',
            twitchUserId: user.twitch_id ?? '',
            aliases: user.aliases,
            lastMessages: [],
            lastStrikeTime: -1,
            penalties: this.penaltyStorageFactory.create()
        };
    }
}
