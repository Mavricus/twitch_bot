import { IUserDto, IUserInfo, IUsersDao } from '../../data_sources/users/user_dao';

export interface IUserDbStorage {
    update(requestId: string, user: IUserInfo): Promise<IUserDto>;
    delete(requestId: string, userId: number): Promise<void>;
    getByTwitchId(requestId: string, userId: string): Promise<IUserDto | null>;
    getByTwitchName(requestId: string, userName: string): Promise<IUserDto | null>;
    create(requestId: string, user: IUserInfo): Promise<IUserDto>;
    addAlias(requestId: string, userId: number, alias: string, streamerId: number): Promise<IUserDto>;
}

export class UserDbStorage implements IUserDbStorage {
    constructor(private userDao: IUsersDao) {
    }

    delete(requestId: string, userId: number): Promise<void> {
        return this.userDao.delete(requestId, userId);
    }

    getByTwitchName(requestId: string, userName: string): Promise<IUserDto | null> {
        if (userName == null) {
            return Promise.resolve(null);
        }
        return this.userDao.getUserByTwitchName(requestId, userName);
    }

    getByTwitchId(requestId: string, userId: string): Promise<IUserDto | null> {
        if (userId == null) {
            return Promise.resolve(null);
        }
        return this.userDao.getUserByTwitchId(requestId, userId);
    }

    addAlias(requestId: string, userId: number, alias: string, streamerId: number): Promise<IUserDto> {
        return this.userDao.addAlias(requestId, alias, userId, streamerId);
    }

    create(requestId: string, user: IUserInfo): Promise<IUserDto> {
        return this.userDao.addUser(requestId, user);
    }

    update(requestId: string, user: IUserInfo): Promise<IUserDto> {
        return this.userDao.update(requestId, user);
    }
}
