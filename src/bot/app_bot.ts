import { Client, Events } from 'tmi.js';

export interface IAppBotFactory {
    start(client: Client): void;
}

export class AppBotFactory implements IAppBotFactory {
    constructor(private eventProcessors: Partial<Events>) {
    }

    start(client: Client) {
        Object.entries(this.eventProcessors)
              .forEach(([event, processor]) => client.on(<keyof Events>event, <(...args: any[]) => any>processor));
    }
}
