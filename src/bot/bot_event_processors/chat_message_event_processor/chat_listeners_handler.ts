import { IUserMessageDetails } from './message_information_builder';

export interface IChatMessageListener {
    (requestId: string, message: IUserMessageDetails): Promise<void>;
}

export interface IChatListenersHandler {
    addMessageListener(listener: IChatMessageListener): void;
    process(requestId: string, message: IUserMessageDetails): Promise<void>;
}

export class ChatListenersHandler implements IChatListenersHandler {
    private listeners: Array<IChatMessageListener> = [];
    
    addMessageListener(listener: IChatMessageListener): void {
        this.listeners.push(listener);
    }

    process(requestId: string, message: IUserMessageDetails): Promise<void> {
        return Promise.all(this.listeners.map(listener => listener(requestId, message)))
                      .then();
    }
}
