import uuid from 'uuid';
import { IStreamerDao, IStreamerDto } from '../../../data_sources/users/streamers_dao';
import { IChatUser } from './chat_message_handlers/moderation/moderate_types';
import { IUserState } from '../user_state_builder';
import { IMessageDto, IUserMessagesDao, UserMessageType } from '../../../data_sources/users/user_messages_dao';
import { IChatUserStorage } from '../../users/chat_user_storage';

const CUSTOM_TYPE_1 = '3e3ee48b-4107-4bac-b89b-491e156ccffb';

export interface IUserMessageDetails {
    id: string;
    sender: IChatUser;
    streamer: IStreamerDto;
    text: string;
    self: boolean;
    timestamp: number;
    messageType: UserMessageType;
    channel: string;
}

export interface IMessageInformationBuilder {
    build(requestId: string, streamerName: string, channel: string, userState: IUserState, message: string, self: boolean): Promise<IUserMessageDetails>;
}

export class MessageInformationBuilder implements IMessageInformationBuilder {
    constructor(private userStorage: IChatUserStorage,
                private streamerDao: IStreamerDao,
                private userMessageDao: IUserMessagesDao) {
    }

    async build(requestId: string, streamerName: string, channel: string, userState: IUserState, message: string, self: boolean): Promise<IUserMessageDetails> {
        const result = await this.buildMessage(userState, channel, requestId, streamerName, message, self);
        await this.saveUserMessage(requestId, result);
        
        return result;
    }

    private async buildMessage(userState: IUserState, channel: string, requestId: string, streamerName: string, message: string, self: boolean): Promise<IUserMessageDetails> {
        return {
            id: userState.id ?? uuid.v1(),
            channel,
            sender: await this.userStorage.get(requestId, userState),
            streamer: await this.findStreamer(requestId, streamerName),
            text: message,
            timestamp: userState.timestamp,
            messageType: MessageInformationBuilder.getMessageType(userState),
            self
        };
    }

    private static getMessageType(userState: IUserState): UserMessageType {
        if (userState.isHighlighted) {
            return UserMessageType.highlighted;
        }

        if (userState.customReward === CUSTOM_TYPE_1) {
            return UserMessageType.custom1;
        }

        return UserMessageType.common;
    }

    private async findStreamer(requestId: string, streamerName: string): Promise<IStreamerDto> {
        const user = await this.userStorage.getByName(requestId, streamerName);
        if (user == null) {
            throw new Error('Cannot find user');
        }

        let streamer = await this.streamerDao.getStreamerByUserId(requestId, user.id);

        if (streamer == null) {
            return await this.streamerDao.addStreamer(requestId, { userId: user.id });
        }

        return streamer;
    }

    private saveUserMessage(messageId: string, message: IUserMessageDetails): Promise<void> {
        const messageInfo: IMessageDto = {
            messageId: messageId,
            userId: message.sender.id,
            streamerId: message.streamer.id,
            timestamp: message.timestamp,
            text: message.text,
            type: message.messageType
        };
        
        return this.userMessageDao.addMessage(messageId, messageInfo);
    }
}
