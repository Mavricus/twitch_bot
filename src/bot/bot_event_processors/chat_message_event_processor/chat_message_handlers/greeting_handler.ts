import { Client } from 'tmi.js';
import format from 'string-format';
import { IUserMessageDetails } from '../message_information_builder';
import { IUserMessageHandler } from './types';
import { IBotCommandMessageScopeBuilder } from './commands/bot_command_mesage_scope_builder';
import { IAppConfig } from '../../../../configuration/corfig_reader';
import { randomInt } from '../../../../utils/number';

export interface IGreetingHandler extends IUserMessageHandler {
    start(): void;
    stop(): void;
}

export class GreetingHandler implements IGreetingHandler {
    private usersList: { [name: string]: boolean } = { };

    private readonly messageTemplates: Array<string> = [
        '@{sender} {senderAlias}, привееееет! Рад тебя тут видеть!',
        '@{sender} {senderAlias}, GivePLZ GivePLZ GivePLZ ',
        '@{sender} {senderAlias}, VoHiYo VoHiYo VoHiYo ',
        '@{sender} {senderAlias}, ееее! И ты тут! Каеф!',
        '@{sender} {senderAlias}, RaccAttack /'
    ]

    constructor(private config: IAppConfig,
                private client: Client,
                private scopeBuilder: IBotCommandMessageScopeBuilder) {
    }

    start() {
    }

    stop() {
        this.usersList = { };                                                                                       
    }

    handle(requestId: string, message: IUserMessageDetails): Promise<void> {
        if (this.usersList[message.sender.name]) {
            return Promise.resolve();
        }

        this.usersList[message.sender.name] = true;

        const template = this.getMessageTemplate();
        const scope = this.scopeBuilder.build(message)
        const messageText = format(template, scope);
        const timeout = randomInt(3_000, 2_000);
        return new Promise<void>(resolve => {
            setTimeout(() => this.client.say(message.channel, messageText).then(() => resolve()), timeout);
        });
    }

    private getMessageTemplate(): string {
        let index = randomInt(this.messageTemplates.length);

        return this.messageTemplates[index];
    }
}
