import { IUserMessageHandler } from './types';
import { IUserMessageDetails } from '../message_information_builder';
import { Client } from 'tmi.js';
import { IAppConfig } from '../../../../configuration/corfig_reader';

export interface IMentioningHandler extends IUserMessageHandler {

}

export class MentioningHandler implements IMentioningHandler {
    private static readonly greetingWords: Array<string> = [
        'привет', 'хай', 'хело', 'хало', 'хелло', 'хелло', 'даров', 'доров', 'дороу', 'hello', '0/'
    ];

    constructor(private config: IAppConfig,
                private client: Client) {
    }

    handle (requestId: string, message: IUserMessageDetails): Promise<void> {
        if (!this.isMentioned(message)) {
            return Promise.resolve();
        }

        if (this.isGreeting(message.text.toLowerCase())) {
            return Promise.resolve();
        }

        return this.client.say(message.channel, `@${message.sender.name} CoolStoryBob `)
                   .then();
    }

    private isGreeting(message: string): boolean {
        return MentioningHandler.greetingWords.some(word => message.includes(word));
    }

    private isMentioned(message: IUserMessageDetails): boolean {
        return message.text.split(' ')
                      .includes(`@${this.config.bot.config.identity?.username}`);
    }
}
