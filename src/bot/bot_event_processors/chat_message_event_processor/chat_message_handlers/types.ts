import { IChatMessageListener } from '../chat_listeners_handler';

export interface IUserMessageHandler {
    handle: IChatMessageListener;
}
