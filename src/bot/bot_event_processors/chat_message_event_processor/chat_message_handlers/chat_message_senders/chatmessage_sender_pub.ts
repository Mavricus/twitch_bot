import { IBotCommandMessageSender } from './types';
import { Client } from 'tmi.js';
import { IUserMessageDetails } from '../../message_information_builder';

export class BotCommandMessageSenderPub implements IBotCommandMessageSender {
    constructor(private chatClient: Client) {
    }
    
    send(requestId: string, message: IUserMessageDetails, response: string): Promise<void> {
        return this.chatClient.say(message.channel, response)
                   .then();
    }
}
