import { IUserMessageDetails } from '../../message_information_builder';

export interface IBotCommandMessageSender {
    send(requestId: string, message: IUserMessageDetails, response: string): Promise<void>;
}
