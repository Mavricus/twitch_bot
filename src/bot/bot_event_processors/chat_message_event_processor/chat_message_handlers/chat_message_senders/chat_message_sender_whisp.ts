import { IBotCommandMessageSender } from './types';
import { IUserMessageDetails } from '../../message_information_builder';
import { Client } from 'tmi.js';

export class BotCommandMessageSenderWhisper implements IBotCommandMessageSender {
    constructor(private chatClient: Client) {
    }

    send(requestId: string, message: IUserMessageDetails, response: string): Promise<void> {
        return this.chatClient.whisper(message.sender.name, response)
                   .then();
    }
}
