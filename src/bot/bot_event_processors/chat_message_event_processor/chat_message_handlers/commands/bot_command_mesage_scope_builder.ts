import { IUserMessageDetails } from '../../message_information_builder';
import { randomInt } from '../../../../../utils/number';

export interface IBotCommandMessageScope {
    sender?: string;
    senderAlias?: string;
    streamer?: string;
    streamerAlias?: string;
}

export interface IBotCommandMessageScopeBuilder {
    build(message: IUserMessageDetails): IBotCommandMessageScope;
}

export class BotCommandMessageScopeBuilder implements IBotCommandMessageScopeBuilder {
    private readonly defaultSenderAliases: Array<string> = ['дружище', 'чувак', 'чел', 'кукусик', 'бритишка'];
    private readonly defaultStreamerAliases: Array<string> = ['стример', 'хозяин'];

    build(message: IUserMessageDetails): IBotCommandMessageScope {
        const senderAlias = BotCommandMessageScopeBuilder.getAlias(message.sender.aliases, this.defaultSenderAliases);
        const streamerAlias = BotCommandMessageScopeBuilder.getAlias(message.streamer.userInfo.aliases, this.defaultStreamerAliases);

        return {
            sender: message.sender.name,
            senderAlias: senderAlias,
            streamer: message.streamer.userInfo.name,
            streamerAlias: streamerAlias
        };
    }

    private static getAlias(aliases: Array<string> | null, defaultAliases: Array<string>): string {
        if (Math.random() < 0.1) {
            return '';
        }
        if (!aliases?.length) {
            return defaultAliases[randomInt(defaultAliases.length - 1)];
        }
        return aliases[randomInt(aliases.length - 1)];
    }
}
