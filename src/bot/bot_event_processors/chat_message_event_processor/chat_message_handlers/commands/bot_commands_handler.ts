import { IBotCommandResponseBuilder } from './message_builders/types';
import format from 'string-format';
import { IBotCommandMessageScopeBuilder } from './bot_command_mesage_scope_builder';
import { IUserMessageDetails } from '../../message_information_builder';
import { IBotCommandMessageSender } from '../chat_message_senders/types';
import { ILogger } from '../../../../../utils/logger/base_logger';

export interface IBotCommandHandler {
    handle(requestId: string, message: IUserMessageDetails): Promise<void>;
    getDescription(): string;
}

export class BotCommandHandler implements IBotCommandHandler {
    constructor(private messageBuilder: IBotCommandResponseBuilder,
                private scopeBuilder: IBotCommandMessageScopeBuilder,
                private sender: IBotCommandMessageSender,
                private logger: ILogger) {
    }

    getDescription(): string {
        return this.messageBuilder.getDescription();
    }

    async handle(requestId: string, message: IUserMessageDetails): Promise<void> {
        try {
            return this.messageBuilder.build(requestId, message)
                      .then(template => this.buildMessage(template, message))
                      .then(messageText => this.sender.send(requestId, message, messageText));
        } catch (err) {
            this.logger.error({ description: 'Cannot handle command request.', requestId, err });
            return this.sender.send(requestId, message, `@${message.sender.name}, отстань! Я занят порабощением мира!`);
        }
    }

    private buildMessage(template: string, message: IUserMessageDetails): string {
        const scope = this.scopeBuilder.build(message);
        return format(template, scope);
    }
}

export class BotCommandHandlerFactory {
    constructor(private scopeBuilder: IBotCommandMessageScopeBuilder,
                private sender: IBotCommandMessageSender,
                private logger: ILogger) {
    }
    
    create(messageBuilder: IBotCommandResponseBuilder) {
        return new BotCommandHandler(messageBuilder, this.scopeBuilder, this.sender, this.logger);
    }
}
