import format from 'string-format';
import { IBotCommandResponseBuilder } from './types';
import { IUserMessageDetails } from '../../../message_information_builder';
import { IObjectStorage } from '../../../../../../utils/storages/storage_types';

export interface IBotCommandCounterConfig {
    description: string;
    messageTemplate: string;
    streamerName: string;
}

export class BotCommandCounterResponseBuilder implements IBotCommandResponseBuilder {
    constructor(private config: IBotCommandCounterConfig, private valueStorage: IObjectStorage<number>) {
    }

    getDescription(): string {
        return this.config.description;
    }

    async build(requestId: string, message: IUserMessageDetails): Promise<string> {
        const count = (await this.valueStorage.get() ?? 0) + 1;
        const result = format(this.config.messageTemplate, { count });
        await this.valueStorage.set(count);
        return result;
    }
}
