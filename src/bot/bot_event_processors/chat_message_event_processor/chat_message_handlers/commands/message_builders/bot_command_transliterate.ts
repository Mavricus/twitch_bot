import { IBotCommandResponseBuilder } from './types';
import { IUserMessageDetails } from '../../../message_information_builder';

interface IAlphabet {
    from: Array<string>,
    to: Array<string>
}

export const RuEngAlphabet: IAlphabet = {
    from: ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\\', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.',
           'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '{', '}',  '|', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':', '"',  'Z', 'X', 'C', 'V', 'B', 'N', 'M', '<', '>',
           '`', '~'],
    to:   ['й', 'ц', 'у', 'к', 'е', 'н', 'г', 'ш', 'щ', 'з', 'х', 'ъ', '\\', 'ф', 'ы', 'в', 'а', 'п', 'р', 'о', 'л', 'д', 'ж', 'э',  'я', 'ч', 'с', 'м', 'и', 'т', 'ь', 'б', 'ю',
           'Й', 'Ц', 'У', 'К', 'Е', 'Н', 'Г', 'Ш', 'Щ', 'З', 'Х', 'Ъ', '/',  'Ф', 'Ы', 'В', 'А', 'П', 'Р', 'О', 'Л', 'Д', 'Ж', 'Э',  'Я', 'Ч', 'С', 'М', 'И', 'Т', 'Ь', 'Б', 'Ю',
           'ё', 'Ё']
};

export class BotCommandTransliterateResponseBuilder implements IBotCommandResponseBuilder {
    private dictionary: {[key: string]: string} = { };

    constructor(alphabet: IAlphabet) {
        if (alphabet.from.length !== alphabet.to.length || !alphabet.from.length) {
            throw new Error('Alphabet is not complete');
        }
        alphabet.from.forEach((from, index) => {
            let to = alphabet.to[index];
            this.dictionary[from] = to;
            this.dictionary[to] = from;
        });
    }

    getDescription(): string {
        return 'Типа пунто свитчер последнего сообщения';
    }
    
    async build(requestId: string, message: IUserMessageDetails): Promise<string> {
        let targetMessage = message.sender.lastMessages[message.sender.lastMessages.length - 1].text;
        
        return `@{sender}, ${this.translate(targetMessage)}`;
    }

    private translate(message: string): string {
        let result = '';
        for (let i = 0; i < message.length; i++) {
            let newChar = this.dictionary[message[i]];
            result += newChar == null ? message[i] : newChar;
        }
        return result;
    }
}
