import format from 'string-format';
import { IBotCommandResponseBuilder } from './types';
import { IUserMessageDetails } from '../../../message_information_builder';
import { IAppConfig } from '../../../../../../configuration/corfig_reader';
import { ITwitchApi } from '../../../../../../twitch_api/twitch_api';
import { randomInt } from '../../../../../../utils/number';

export class BotCommandGameResponseBuilder implements IBotCommandResponseBuilder {
    private onlineMessages: Array<string> = [
        '@{{sender}} {{senderAlias}}, да какая разница, все разно стрима нет BibleThump BibleThump BibleThump ',
        '@{{sender}} {{senderAlias}}, нету стрима, какая разница что там написано?'
    ];
    
    private offlineMessages: Array<string> = [
        '@{{sender}} {{senderAlias}}, ну написано же, "{game}"!',
        '@{{sender}} {{senderAlias}}, говорят, что {game}.'
    ];

    constructor(private config: IAppConfig, private twitchApiClient: ITwitchApi) {
    }

    getDescription(): string {
        return 'Чем занимаемся?';
    }

    async build(requestId: string, message: IUserMessageDetails): Promise<string> {
        const name = this.config.bot.options.streamersName;
        if (!await this.twitchApiClient.isStreamOnlineByName(name)) {
            return BotCommandGameResponseBuilder.getMessage(this.offlineMessages, '');
        }
        let channelInfo = await this.twitchApiClient.getChannelByUserName(name);
        if(!channelInfo) {
            throw new Error('Cannot getByTwitchId channel information');
        }
        return BotCommandGameResponseBuilder.getMessage(this.onlineMessages, channelInfo.game);
    }

    private static getMessage(messages: Array<string>, game: string): string {
        return format(messages[randomInt(messages.length)], { game });
    }
}
