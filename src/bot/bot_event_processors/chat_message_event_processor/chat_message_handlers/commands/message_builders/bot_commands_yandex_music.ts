import { IBotCommandResponseBuilder } from './types';
import { IUserMessageDetails } from '../../../message_information_builder';
import { IMusicDao } from '../../../../../../data_sources/music_dao';

export class BotCommandYandexMusicResponseBuilder implements IBotCommandResponseBuilder {
    private readonly baseUrl = 'https://music.yandex.ru/search?text=';

    constructor(private musicStorage: IMusicDao) {
    }

    getDescription(): string {
        return 'А есть такая же, но на яндекс музыке?';
    }

    async build(requestId: string, message: IUserMessageDetails): Promise<string> {
        let music = await this.musicStorage.get();
        if (music == null) {
            throw new Error('Cannot find music information');
        }
        let url = this.baseUrl + escape(`${music.artist} - ${music.track}`);
        return `@{sender} {senderAlias}, может тут найдется, ${url}`;
    }
}
