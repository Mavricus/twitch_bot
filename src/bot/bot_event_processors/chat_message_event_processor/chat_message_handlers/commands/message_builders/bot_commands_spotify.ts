import { IBotCommandResponseBuilder } from './types';
import { IUserMessageDetails } from '../../../message_information_builder';
import { IMusicDao } from '../../../../../../data_sources/music_dao';

export class BotCommandSpotifyResponseBuilder implements IBotCommandResponseBuilder {
    private readonly baseUrl = 'https://open.spotify.com/search/';

    constructor(private musicStorage: IMusicDao) {
    }

    getDescription(): string {
        return 'А есть такаяже но в Spotity?';
    }

    async build(requestId: string, message: IUserMessageDetails): Promise<string> {
        let music = await this.musicStorage.get();
        if (music == null) {
            throw new Error('Cannot getByTwitchId the music information');
        }
        let url = this.baseUrl + escape(`${music.artist} - ${music.track}`);
        return `@{sender} {senderAlias}, может тут найдется, ${url}`;
    }
}
