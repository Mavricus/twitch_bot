import { IUserMessageDetails } from '../../../message_information_builder';

export interface IBotCommandResponseBuilder {
    build(requestId: string, message: IUserMessageDetails): Promise<string>;
    getDescription(): string;
}
