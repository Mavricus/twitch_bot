import { IBotCommandResponseBuilder } from './types';
import { IBotCommandsCollection } from '../bot_command_collection';
import format from 'string-format';
import { IUserMessageDetails } from '../../../message_information_builder';
import { randomInt } from '../../../../../../utils/number';

export class BotCommandHelpResponseBuilder implements IBotCommandResponseBuilder {
    private messages: Array<string> = [
        '@{{sender}}, ну смори: {info}',
        '@{{sender}} {{senderAlias}}, ну смори: {info}'
    ];

    constructor(private commandsCollection: IBotCommandsCollection) {
    }

    getDescription(): string {
        return 'Печатает вот это сообщение.';
    }

    build(requestId: string, message: IUserMessageDetails): Promise<string> {
        const info  = this.commandsCollection.getCommandsInfo()
                       .map(({ triggers, description }) => `${triggers[0]} - ${description}`)
                       .join('; ');

        return Promise.resolve(BotCommandHelpResponseBuilder.getMessage(this.messages, info));
    }

    private static getMessage(messages: Array<string>, info: string): string {
        return format(messages[randomInt(messages.length)], { info });
    }
}
