import { IBotCommandResponseBuilder } from './types';
import format from 'string-format';
import { IUserMessageDetails } from '../../../message_information_builder';
import { IAppConfig } from '../../../../../../configuration/corfig_reader';
import { ITwitchApi } from '../../../../../../twitch_api/twitch_api';
import { getLocalizedTime } from '../../../../../../utils/date_time';
import { randomInt } from '../../../../../../utils/number';

export class BotCommandUptimeResponseBuilder implements IBotCommandResponseBuilder {
    private offlineMessages: Array<string> = [
        '@{{sender}}, казалось бы, всего {time} стрима нет, а как буд-то вечность прошла...',
        '@{{sender}} {{senderAlias}}, я его уже {time} как тут не наблюдаю!',
        '@{{sender}} {{senderAlias}}, не знаю, долго это или нет, но стрима уже {time} как нету.'
    ];
    
    private onlineMessages: Array<string> = [
        '@{{sender}} у нас тут уже {time} непрерывное веселья!',
        '@{{sender}} {{senderAlias}}, ты не поверишь, мы уже тут {time} сидим-тусим!',
        '@{{sender}} {{senderAlias}}, да {time} сидим тут пердим.'
    ];

    constructor(private config: IAppConfig,
                private twitchApi: ITwitchApi) {
    }

    getDescription(): string {
        return 'И сколько вы тут уже тупите?';
    }

    async build(requestId: string, message: IUserMessageDetails): Promise<string> {
        if (await this.twitchApi.isStreamOnlineByName(this.config.bot.options.streamersName)) {
            return this.buildOnlineTime(message.streamer.lastOnline);
        }
        return this.buildOfflineTime(message.streamer.lastOffline);
    }

    private buildOnlineTime(startTime: number | null): string {
        if (startTime == null) {
            throw new Error('Cannot getByTwitchId a stream start time');
        }
        const time = getLocalizedTime(Date.now() - startTime);
        return BotCommandUptimeResponseBuilder.getMessage(this.onlineMessages, time);
    }

    private buildOfflineTime(stopTime: number | null): string {
        if (stopTime == null) {
            throw new Error('Cannot getByTwitchId a stream stop time');
        }
        const time = getLocalizedTime(Date.now() - stopTime);
        return BotCommandUptimeResponseBuilder.getMessage(this.offlineMessages, time);
    }

    private static getMessage(messages: Array<string>, time: string): string {
        return format(messages[randomInt(messages.length)], { time });
    }
}
