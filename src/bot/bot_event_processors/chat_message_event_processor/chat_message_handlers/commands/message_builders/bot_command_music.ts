import format from 'string-format';
import { IBotCommandResponseBuilder } from './types';
import { IUserMessageDetails } from '../../../message_information_builder';
import { IAppConfig } from '../../../../../../configuration/corfig_reader';
import { IMusic } from '../../../../../../server/routes/music_get';
import { ITwitchApi } from '../../../../../../twitch_api/twitch_api';
import { randomInt } from '../../../../../../utils/number';
import { IMusicDao } from '../../../../../../data_sources/music_dao';

export class BotCommandMusicResponseBuilder implements IBotCommandResponseBuilder {
    private onlineMessages = [
        '@{{sender}} {{senderAlias}}, кажется, что это {artist} - {track} {{senderAlias}}',
        '@{{sender}} {{senderAlias}}, да это же {artist} - {track}',
        '@{{sender}} {{senderAlias}}, это мой любимы трек, {artist} - {track}',
        '@{{sender}} {{senderAlias}}, мой любимы трек {artist} - {track}'
    ];

    private offlineMessages = [
        '@{sender} {senderAlias}, поздравляю! У тебя галлюцинации! Стрима нет, какая музыка?!',
        '@{sender} {senderAlias}, тут полная тишина. Ничего нет.',
        '@{sender} {senderAlias}, нету стрима - нет музыки.'
    ];

    constructor(private config: IAppConfig,
                private musicStorage: IMusicDao,
                private twitchApi: ITwitchApi) {
    }

    getDescription(): string {
        return 'Что за музыка играет?';
    }

    async build(requestId: string, message: IUserMessageDetails): Promise<string> {
        if (!await this.twitchApi.isStreamOnlineByName(this.config.bot.options.streamersName)) {
            return this.offlineMessages[randomInt(this.offlineMessages.length)];
        }
        let music = await this.musicStorage.get();
        if (music == null) {
            throw new Error('Cannot find music information');
        }

        return BotCommandMusicResponseBuilder.getMessage(this.onlineMessages, music);
    }

    private static getMessage(messages: Array<string>, music: IMusic): string {
        return format(messages[randomInt(messages.length)], music);
    }
}
