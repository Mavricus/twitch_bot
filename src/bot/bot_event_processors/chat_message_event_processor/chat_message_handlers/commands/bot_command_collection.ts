import { IBotCommandHandler } from './bot_commands_handler';

export interface ICommandInfo {
    triggers: Array<string>;
    description: string;
}

export interface IBotCommandsCollection {
    registerCommand(triggers: Array<string>, command: IBotCommandHandler): void;
    getCommandsInfo(): Array<ICommandInfo>;
    getCommand(trigger: string): IBotCommandHandler | undefined | null;
}

export class BotCommandCollection implements IBotCommandsCollection {
    private commandsMap: Map<string, IBotCommandHandler> = new Map<string, IBotCommandHandler>();
    private commandsInfo: Array<ICommandInfo> = [];

    getCommandsInfo(): Array<ICommandInfo> {
        return this.commandsInfo;
    }

    registerCommand(triggers: Array<string>, command: IBotCommandHandler): void {
        this.commandsInfo.push({ triggers, description: command.getDescription() });
        triggers.forEach(trigger => this.commandsMap.set(trigger, command));
    }

    getCommand(trigger: string): IBotCommandHandler | undefined | null {
        return this.commandsMap.get(trigger);
    }
}
