import { BotCommandCollection } from './bot_command_collection';
import { IUserMessageDetails } from '../../message_information_builder';
import { IUserMessageHandler } from '../types';
import { ILogger } from '../../../../../utils/logger/base_logger';

export interface IBotCommandsPerformer extends IUserMessageHandler {
}

export class BotCommandsPerformer implements IBotCommandsPerformer {
    constructor(private commandsCollection: BotCommandCollection,
        private logger: ILogger) {
    }

    async handle(requestId: string, message: IUserMessageDetails): Promise<void> {
        let triggers = BotCommandsPerformer.findTriggers(message.text);
        
        if (!triggers.length) {
            return;
        }

        this.logger.debug({ description: 'User has sent commands', triggers, requestId, user: message.sender });
        triggers.map(trigger => {
            return {
                trigger,
                command: this.commandsCollection.getCommand(trigger)
            };
        }).forEach(({ command, trigger }) => {
            if (!command) {
                this.logger.debug({ description: 'No commands found for trigger', trigger, requestId });
                return;
            }
            this.logger.debug({ description: 'About to process the trigger', trigger, requestId });
            command.handle(requestId, message);
        });
    }

    private static findTriggers(message: string): Array<string> {
        return message.toLowerCase()
                      .split(' ')
                      .filter(word => word.startsWith('!'));
    }
}
