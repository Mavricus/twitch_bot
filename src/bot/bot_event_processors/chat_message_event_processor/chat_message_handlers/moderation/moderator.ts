import { Client } from 'tmi.js';
import { IModerateAction, IViolationPenalty } from './moderate_types';
import { IPunisher } from './punishment/punisher';
import { IUserMessageDetails } from '../../message_information_builder';
import { IUserMessageHandler } from '../types';
import { IAppConfig } from '../../../../../configuration/corfig_reader';
import { ILogger } from '../../../../../utils/logger/base_logger';

export interface IModerator extends IUserMessageHandler {
    registerModeratorAction(...actions: Array<IModerateAction>): void;
}

export class Moderator implements IModerator {
    private actions: Array<IModerateAction> = [];

    constructor(private config: IAppConfig,
                private punisher: IPunisher,
                private client: Client,
                private logger: ILogger) {
    }

    async handle(requestId: string, message: IUserMessageDetails): Promise<void> {
        this.logger.trace({ requestId, description: 'User has send a message', message });

        const tasks = this.actions.map(action => action.moderateMessage(requestId, message.sender, message.text));

        let messagePenalties = <Array<IViolationPenalty>>(await Promise.all(tasks)).filter(penalty => penalty != null);

        //TODO 11/7/2019 - Mavricus: Add messages cleanup logic. Create selfcontrolled UserStatistic class
        message.sender.lastMessages.push({ time: Date.now(), text: message.text });

        if (!messagePenalties.length) {
            return;
        }
        try {
            let rules = messagePenalties.map(penalty => penalty.shortDescription);
            this.logger.debug({ description: 'User has violated rules', requestId, user: message.sender, rules });
            return await this.punisher.process(requestId, message.channel, message.sender, messagePenalties);
        } catch (err) {
            this.client.say(message.channel, `@${this.config.bot.options.streamersName}, тут @${message.sender.name} напрашивается.`).then();
            throw err;
        }
    }

    registerModeratorAction(...actions: Array<IModerateAction>): void {
        actions.forEach(action => this.actions.push(action));
    }
}
