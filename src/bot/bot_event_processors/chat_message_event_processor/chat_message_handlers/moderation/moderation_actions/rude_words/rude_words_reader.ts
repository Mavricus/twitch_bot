import { IDictionary, IDictionaryFactory } from '../../../../../../../utils/collections';
import { IRudeWord, IRudeWordsDto } from '../../../../../../../data_sources/rode_words_dao';

export interface IRudeWordsProvider {
    getDictionary(requestId: string): Promise<IDictionary>;
    updateDictionary(requestId: string): Promise<IDictionary>
}

export class RudeWordsProvider implements IRudeWordsProvider {
    private dictionary: IDictionary | null = null;

    constructor(private dictionaryFactory: IDictionaryFactory, private wordsDto: IRudeWordsDto) {
    }

    getDictionary(requestId: string): Promise<IDictionary> {
        if (this.dictionary) {
            return Promise.resolve(this.dictionary);
        }
        
        return this.updateDictionary(requestId);
    }

    updateDictionary(requestId: string): Promise<IDictionary> {
        return this.wordsDto.getAll(requestId)
                   .then(data => this.configDictionary(data))
                   .then(dictionary => this.dictionary = dictionary);
    }

    private configDictionary(words: Array<IRudeWord>): IDictionary {
        const dictionary = this.dictionaryFactory.create();
        words.forEach(info => dictionary.set<IRudeWord>(info.word, info));
        
        return dictionary;
    }
}
