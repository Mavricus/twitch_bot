import { IChatUser, IModerateAction, IViolationPenalty, Penalties } from '../../moderate_types';
import { IRudeWordsProvider } from './rude_words_reader';
import { IRudeWord } from '../../../../../../../data_sources/rode_words_dao';

export class ModerateRudeWords implements IModerateAction {
    constructor(private wordsProvider: IRudeWordsProvider) {
    }

    async moderateMessage(requestId: string, user: IChatUser, message: string): Promise<IViolationPenalty | null> {
        const dictionary = await this.wordsProvider.getDictionary(requestId);
        let violations = (await dictionary.getKeys()).filter(word => message.toLowerCase().includes(word));
        let info = <Array<IRudeWord>>(await Promise.all(violations.map(word => dictionary.get<IRudeWord>(word))));
        if (!info.length) {
            return null;
        }
        return ModerateRudeWords.definePenalty(info);
    }

    private static definePenalty(words: Array<IRudeWord>): IViolationPenalty {
        return words.reduce<IViolationPenalty>((penalty, word) => {
            penalty.penaltyScore += word.penaltyScore;
            penalty.penaltyTtl += word.penaltyTtl;
            penalty.timeoutValue += word.timeoutValue;
            
            if (word.penaltyType > penalty.penaltyType) {
                penalty.penaltyType = word.penaltyType;
                penalty.warningMessage = word.warningMessage || penalty.warningMessage;
            }
            return penalty;
        }, {
            shortDescription: 'rude words',
            warningMessage: 'Попрошу не выражаться!',
            penaltyTtl: 0,
            penaltyType: Penalties.none,
            timeoutValue: 0,
            penaltyScore: 0,
            penaltyTimestamp: Date.now()
        });
    }
}
