import { IChatUser, IModerateAction, IViolationPenalty, Penalties } from '../moderate_types';

export class ModerateCapsLock implements IModerateAction {
    async moderateMessage(requestId: string, user: IChatUser, message: string):  Promise<IViolationPenalty | null> {
        if (message.length < 7 || message != message.toUpperCase()) {
            return null;
        }
        return {
            penaltyType: Penalties.timeout,
            penaltyScore: 50,
            timeoutValue: 30,
            warningMessage: `чо ты орешь, таймача давно не было?`,
            shortDescription: 'Caps Lock',
            penaltyTimestamp: Date.now(),
            penaltyTtl: 2 * 60
        };
    }
}
