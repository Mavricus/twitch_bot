import { IChatUser, Penalties } from '../moderate_types';
import { IPunishment } from './punishment_caclulator';
import { Client } from 'tmi.js';

export interface IPunishmentExecutor {
    punish(requestId: string, channel: string, user: IChatUser, punishment: IPunishment): Promise<void>;
}

export class PunishmentExecutor implements IPunishmentExecutor {
    //TODO: At the moment timeout throws an error even in positive scenario. Worth to test twitchApi instead of client.
    constructor(private client: Client) {
    }

    punish(requestId: string, channel: string, user: IChatUser, punishment: IPunishment): Promise<void> {
        switch (punishment.condemnation) {
            case Penalties.none: return Promise.resolve();
            case Penalties.verbal: return this.warnUser(channel, user, punishment);
            case Penalties.timeout: return this.timeoutUser(channel, user, punishment);
            case Penalties.ban: return this.banUser(channel, user, punishment);
            default: throw new Error('Unsupportable condemnation type')
        }
    }

    private warnUser(channel: string, user: IChatUser, punishment: IPunishment): Promise<void> {
        return this.client.say(channel, `@${user.name}, ${punishment.reason}`).then();
    }

    private timeoutUser(channel: string, user: IChatUser, punishment: IPunishment): Promise<void> {
        return Promise.all([
            this.warnUser(channel, user, punishment),
            this.client.timeout(channel, user.name, Math.round(punishment.time))
        ]).then();
    }

    private banUser(channel: string, user: IChatUser, punishment: IPunishment): Promise<void> {
        return Promise.all([
            this.warnUser(channel, user, punishment),
            this.client.ban(channel, user.name, punishment.reason)
        ]).then();
    }
}
