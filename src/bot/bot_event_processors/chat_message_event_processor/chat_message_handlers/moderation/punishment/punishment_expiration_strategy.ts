import { IViolationPenalty } from '../moderate_types';
import { InMemoryTemporaryStorage } from '../../../../../../utils/storages/temporary_storage';
import { ITypedDataStorage, ITypedDataStorageFactory } from '../../../../../../utils/storages/storage_types';

export class PunishmentExpirationStrategy extends InMemoryTemporaryStorage<IViolationPenalty> {
    protected getExpirationTime(data: IViolationPenalty) {
        return  data.penaltyTtl * 1000;
    }
}

export class TemporaryPenaltyStorageFactory implements ITypedDataStorageFactory<IViolationPenalty> {
    create(): ITypedDataStorage<IViolationPenalty> {
        return new PunishmentExpirationStrategy();
    }
}
