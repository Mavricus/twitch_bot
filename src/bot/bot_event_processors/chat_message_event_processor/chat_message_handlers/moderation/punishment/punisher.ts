import { IPunishmentCalculator } from './punishment_caclulator';
import { IChatUser, IViolationPenalty } from '../moderate_types';
import { IPunishmentExecutor } from './punisment_executor';
import { ILogger } from '../../../../../../utils/logger/base_logger';

export interface IPunisher {
    process(requestId: string, channel: string, user: IChatUser, penalties: Array<IViolationPenalty>): Promise<void>;
}

export class Punisher implements IPunisher {
    constructor(private calculator: IPunishmentCalculator,
                private executor: IPunishmentExecutor,
                private logger: ILogger) {
    }
    
    async process(requestId: string, channel: string, user: IChatUser, penalties: Array<IViolationPenalty>): Promise<void> {
        const punishment = await this.calculator.sumPenalties(requestId, user, penalties);
        this.logger.debug({ description: 'About to process user punishments', requestId, user, punishment });
        if (punishment == null) {
            return ;
        }
        this.logger.debug({ description: 'About to punish user', requestId, user, punishment });
        return this.executor.punish(requestId, channel, user, punishment);
    }
}
