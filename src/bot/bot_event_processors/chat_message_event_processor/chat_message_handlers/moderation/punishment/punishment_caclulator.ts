import { IChatUser, IViolationPenalty, Penalties } from '../moderate_types';
import { ILogger } from '../../../../../../utils/logger/base_logger';

export interface IPunishment {
    condemnation: Penalties;
    time: number;
    reason: string;
}

export interface IPunishmentCalculator {
    sumPenalties(requestId: string, user: IChatUser, penalties: Array<IViolationPenalty>): Promise<IPunishment | null>;
}

export class PunishmentCalculator implements IPunishmentCalculator {
    private readonly MAX_PENALTY_SCORE = 1000;

    constructor(private logger: ILogger) {
    }

    async sumPenalties(requestId: string, user: IChatUser, penalties: Array<IViolationPenalty>): Promise<IPunishment | null> {
        if (!penalties.length) {
            throw new Error('Penalties list is empty');
        }
        
        let banPenalty = penalties.find(penalty => penalty.penaltyType === Penalties.ban);

        if (banPenalty != null) {
            return PunishmentCalculator.buildBanMessage(banPenalty);
        }

        await Promise.all(penalties.map(penalty => user.penalties.add(penalty))).then();

        let [maxPenalty, penaltyScore, timeout] = await this.getPenaltiesMetrics(requestId, user);

        if (penaltyScore > this.MAX_PENALTY_SCORE) {
            return PunishmentCalculator.buildTimeoutPunishment(maxPenalty, timeout);
        }

        if (penaltyScore > (this.MAX_PENALTY_SCORE / 2)) {
            return PunishmentCalculator.buildVerbalPunishment(maxPenalty);
        }

        return null;
    }

    private static buildBanMessage(banPenalty: IViolationPenalty): IPunishment {
        return {
            reason: banPenalty.warningMessage + ' Беги, беги и никогда не возвращайся!',
            condemnation: Penalties.ban,
            time: Infinity
        };
    }

    private static buildVerbalPunishment(penalty: IViolationPenalty): IPunishment {
        return {
            time: 0,
            condemnation: Penalties.verbal,
            reason: `${penalty.warningMessage} Я за тобой слежу!`
        };
    }

    private static buildTimeoutPunishment(penalty: IViolationPenalty, timeout: number): IPunishment {
        return {
            time: timeout,
            condemnation: Penalties.timeout,
            reason: `${penalty.warningMessage} Давай-ка я тебя затаймачу немножко...`
        };
    }

    private async getPenaltiesMetrics(requestId: string, user: IChatUser): Promise<[IViolationPenalty, number, number]> {
        let now = Date.now();
        let penalties = await user.penalties.getAll() || [];
        this.logger.trace({ description: 'All penalties of user', requestId, user, penalties });
        return penalties.reduce((info, item) => {
            if (info[0].penaltyScore > item.penaltyScore) {
                info[0] = item;
            }
            info[1] += item.penaltyScore;
            info[2] += (1 - (now - item.penaltyTimestamp) / 1000 / item.penaltyTtl) * item.timeoutValue;
            return info;
        }, [penalties[0], 0, 0]);
    }
}
