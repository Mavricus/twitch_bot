import { ITypedDataStorage } from '../../../../../utils/storages/storage_types';

export interface IChatUser {
    id: number;
    twitchUserId: string;
    name: string;
    aliases: Array<string>;
    lastMessages: Array<{ time: number, text: string }>;
    penalties: ITypedDataStorage<IViolationPenalty>;
    lastStrikeTime: number;
}

export enum Penalties {
    none,
    verbal,
    timeout,
    ban
}

export interface IViolationPenalty {
    warningMessage: string;
    timeoutValue: number; // in seconds
    penaltyScore: number;
    penaltyType: Penalties;
    shortDescription: string;
    penaltyTimestamp: number;
    penaltyTtl: number; // in seconds
}

export interface IModerateAction {
    moderateMessage(requestId: string, user: IChatUser, message: string):  Promise<IViolationPenalty | null>;
}
