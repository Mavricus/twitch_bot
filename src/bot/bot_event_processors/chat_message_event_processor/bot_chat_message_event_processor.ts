import { IAppConfig } from '../../../configuration/corfig_reader';
import { ILogger } from '../../../utils/logger/base_logger';
import { ChatUserstate } from 'tmi.js';
import { IMessageInformationBuilder } from './message_information_builder';
import { IChatListenersHandler } from './chat_listeners_handler';
import { IUserStateBuilder } from '../user_state_builder';
import * as uuid from 'uuid';

export interface IChatMessageProcessor {
    chat(channel: string, userState: ChatUserstate, message: string, self: boolean): void;
}

export class ChatMessageProcessor implements IChatMessageProcessor {
    constructor(private config: IAppConfig,
                private messageBuilder: IMessageInformationBuilder,
                private listenersHandler: IChatListenersHandler,
                private userStateBuilder: IUserStateBuilder,
                private logger: ILogger) {
    }

    chat(channel: string, userState: ChatUserstate, message: string, self: boolean) {
        if (self) {
            return;
        }

        let requestId = uuid.v1();
        const user = this.userStateBuilder.setCommonState(userState)
                         .setUserName(userState.username)
                         .build();

        return this.messageBuilder.build(requestId, this.config.bot.options.streamersName, channel, user, message, self)
                   .then(message => message.self ? Promise.resolve() : this.listenersHandler.process(requestId, message))
                   .catch(err => this.logger.error({ description: 'Error occurred during message processing', requestId, err }));
    }
}
