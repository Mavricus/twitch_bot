import { Client, SubMethods, SubUserstate } from 'tmi.js';
import uuid from 'uuid';
import { IUserState, IUserStateBuilder } from '../user_state_builder';
import { ILogger } from '../../../utils/logger/base_logger';

export interface ISubscriptionProcessor {
    process(channel: string, username: string, methods: SubMethods, message: string, userState: SubUserstate): void;
}

export enum SubscriptionType { unknown, sub, reSub }

export class BotSubscriptionEventProcessor implements ISubscriptionProcessor {
    constructor(private client: Client,
                private userStateBuilder: IUserStateBuilder,
                private logger: ILogger) {
    }

    process(channel: string, username: string, methods: SubMethods, message: string, userState: SubUserstate) {
        let requestId = uuid.v1();
        const user = this.userStateBuilder.setCommonState(userState)
                         .setUserName(username)
                         .build();
        
        const subType = BotSubscriptionEventProcessor.getSubType(userState);
        this.handleSub(channel, subType, user)
            .catch(err => this.logger.error({ description: 'Error occurred during message processing', requestId, err }));

    }

    private static getSubType(userState: SubUserstate): SubscriptionType {
        if (userState['message-type'] == null) {
            return SubscriptionType.unknown;
        }
        if (userState['message-type'] === 'sub') {
            return SubscriptionType.sub;
        }
        if (userState['message-type'] === 'resub') {
            return SubscriptionType.reSub;
        }
        return SubscriptionType.unknown;
    }

    private handleSub(channel: string, subType: SubscriptionType, user: IUserState): Promise<void> {
        switch (subType) {
            case SubscriptionType.reSub:
                return this.client.say(channel, `НИЧЕГО СЕБЕ! Да это же ${user.name} в который раз подписался! Вот это флекс!`)
                           .then();
            case SubscriptionType.sub:
                return this.client.say(channel, `У нас тут отважный человек появился! ${user.name} решил подписаться! Ну что же, welcome to the club!`)
                           .then();
            default:
                return this.client.say(channel, `Да это же ${user.name} подписался! Вот это флекс!`)
                           .then();
        }
    }
}
