import { IAppConfig } from '../../../configuration/corfig_reader';
import { ILogger } from '../../../utils/logger/base_logger';
import { Client } from 'tmi.js';
import uuid from 'uuid';

export interface IBotRaidedEventProcessor {
    raided(channel: string, username: string, viewers: number): void;
}

export class BotRaidedEventProcessor implements IBotRaidedEventProcessor {
    constructor(private config: IAppConfig,
                private client: Client,
                private logger: ILogger) {
    }

    raided(channel: string, username: string, viewers: number): void {
        let requestId = uuid.v1();

        this.client.say(channel, `${this.config.bot.options.streamersName}, ты чо сидишь, тут ${username} тебя рэйдит. Да еще и с гурьбой в ${viewers} человек!`)
              .catch(err => this.logger.error({ description: 'Error occurred during message processing', requestId, err }));
    }
}
