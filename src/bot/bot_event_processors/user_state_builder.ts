import { CommonUserstate } from 'tmi.js';

const HIGHLIGHTED_MESSAGE = 'highlighted-message';

export interface IUserState {
    name: string;
    id: string;
    customReward: string;
    isSubscriber: boolean;
    isFollower: boolean;
    timestamp: number;
    isHighlighted: boolean;
}

export interface IUserStateBuilder {
    setCommonState(state: CommonUserstate): IUserStateBuilder;
    setUserName(name?: string): IUserStateBuilder;
    setId(id?: string): IUserStateBuilder;
    setCustomReward(customReward?: string): IUserStateBuilder;
    setIsSubscriber(isSubscriber?: boolean): IUserStateBuilder;
    setIsFollower(isFollower?: boolean): IUserStateBuilder;
    setTimestamp(timestamp?: number): IUserStateBuilder;
    setIsHighlighted(isHighlighted?: boolean): IUserStateBuilder;
    build(): IUserState;
}

export class UserStateBuilder implements IUserStateBuilder {
    private memo: IUserState;

    constructor() {
        this.memo = UserStateBuilder.getDefaultState();
    }

    setCommonState(state: CommonUserstate): IUserStateBuilder {
        this.setId(state['user-id'])
            .setIsSubscriber(state.subscriber)
            .setIsHighlighted(state['msg-id'] === HIGHLIGHTED_MESSAGE)
            .setCustomReward(state['custom-reward-id']);

        if (state['tmi-sent-ts'] != null) {
            this.setTimestamp(+state['tmi-sent-ts']);
        }

        return this;
    }

    setCustomReward(customReward?: string): IUserStateBuilder {
        this.memo.customReward = customReward ?? this.memo.customReward;

        return this;
    }

    setUserName(name?: string): IUserStateBuilder {
        this.memo.name = name ?? this.memo.name;

        return this;
    }

    setId(id?: string): IUserStateBuilder {
        this.memo.id = id ?? this.memo.id;

        return this;
    }

    setIsFollower(isFollower?: boolean): IUserStateBuilder {
        this.memo.isFollower = isFollower ?? this.memo.isFollower;

        return this;
    }

    setIsSubscriber(isSubscriber?: boolean): IUserStateBuilder {
        this.memo.isSubscriber = isSubscriber ?? this.memo.isSubscriber;

        return this;
    }

    setTimestamp(timestamp?: number): IUserStateBuilder {
        this.memo.timestamp = timestamp ?? this.memo.timestamp;

        return this;
    }

    setIsHighlighted(isHighlighted?: boolean): IUserStateBuilder {
        this.memo.isHighlighted = isHighlighted ?? this.memo.isHighlighted;

        return this;
    }

    build(): IUserState {
        const result = this.memo;
        this.memo = UserStateBuilder.getDefaultState();

        return result;
    }

    private static getDefaultState(): IUserState {
        return {
            id: '',
            name: '',
            customReward: '',
            isHighlighted: false,
            isFollower: false,
            isSubscriber: false,
            timestamp: 0,
        };
    }
}
