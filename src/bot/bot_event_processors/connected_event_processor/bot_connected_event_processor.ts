import { Client } from 'tmi.js';

export interface IBotConnectedEventProcessor {
    connected(address: string, port: number): void;
}

export interface IBotConnectedEventProcessorConfig {
    channel: string;
}

export class BotConnectedEventProcessor implements IBotConnectedEventProcessor {
    constructor(private config: IBotConnectedEventProcessorConfig, private client: Client) {
    }

    connected(address: string, port: number) {
        this.client.say(this.config.channel, 'Всем сохранять спокойствие! В чате работают профессионалы Kappa Kappa Kappa ');
    };
}
