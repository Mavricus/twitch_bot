import { IMusicDao } from '../../data_sources/music_dao';
import { clearInterval } from 'timers';
import { IMusic } from '../../server/routes/music_get';
import SpotifyWebApi from 'spotify-web-api-node';
import { ILogger } from '../../utils/logger/base_logger';


export interface ISpotifyMusicDiscovery {
    start(): void;
    stop(): void;
}

export interface ISpotifyMusicDiscoveryConfig {
    timeout: number;
    systemId: string;
}

export class SpotifyMusicDiscovery implements ISpotifyMusicDiscovery {
    private timer: NodeJS.Timeout | null = null;

    constructor(private config: ISpotifyMusicDiscoveryConfig,
                private spotifyClient: SpotifyWebApi,
                private musicStorage: IMusicDao,
                private logger: ILogger) {
    }

    start(): void {
        if (this.timer != null) {
            return;
        }

        this.timer = setInterval(() => this.discoverCurrentMusic(), this.config.timeout);
    }

    stop(): void {
        if (this.timer == null) {
            return;
        }

        clearInterval(this.timer);
        this.timer = null;
    }

    private discoverCurrentMusic() {
        Promise.all([
            this.spotifyClient.getMyCurrentPlayingTrack(),
            this.musicStorage.get()
        ]).then(([info, music]) => {
            if (!info?.body?.is_playing) {
                return Promise.resolve();
            }
            let artist: string = 'Unknown Artist';
            const track = info?.body?.item?.name ?? 'Unknown Track';
            const artists = info?.body?.item?.artists?.map(({ name }) => name);
            if (artists != null && artists.length) {
                artist = artists.join(', ');
            }

            if (artist === music?.artist && track == music?.track) {
                return Promise.resolve();
            }

            return this.musicStorage.set({ artist, track })
                .then();
        }).catch(err => {
            this.logger.error({ requestId: this.config.systemId, description: 'Cannot get the Spotify currently playing music', err });
        })
    }
}
