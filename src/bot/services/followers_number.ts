import { ITwitchApi } from '../../twitch_api/twitch_api';
import { IAppConfig } from '../../configuration/corfig_reader';
import { writeFile } from 'fs';

interface Fs {
    writeFile: typeof writeFile;
}

export interface IFollowersNumber {
    updateFollowersNumber(): Promise<void>;
}

export class FollowersNumber implements IFollowersNumber {
    private readonly followersFile: string;
    private readonly followersExpected: number;

    constructor(private config: IAppConfig, private api: ITwitchApi, private fs: Fs) {
        this.followersFile = config.bot.options.followersFile;
        this.followersExpected = config.bot.options.followersExpected;
    }

    updateFollowersNumber(): Promise<void> {
        return this.api.getChannelByUserName(this.config.bot.options.streamersName)
                   .then(channel => channel?.followers ?? 0)
                   .then(number => this.updateFile(number));
    }

    private updateFile(number: number): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.fs.writeFile(this.followersFile, `Follows: ${number}/${this.followersExpected}`, err => {
                if (err) {
                    return reject(err);
                }
                resolve();
            });
        });
    }
}
