import { ITwitchApi } from '../../../twitch_api/twitch_api';
import EventEmitter = NodeJS.EventEmitter;
import { IStreamStatusController, IStreamStatusControllerFactory, NewStreamState } from './stream_status_controller';
import { IStreamerDao, IStreamerDto } from '../../../data_sources/users/streamers_dao';
import uuid from 'uuid';

type StreamStateEvent = 'online' | 'offline' | 'recover' | 'disconnect';

export interface IStreamDiscoveryService {
    start(): void;
    stop(): void;
    on(state: StreamStateEvent, handler: () => void): void;
}

interface IStreamDiscoveryServiceConfig {
    streamers: Array<string>;
    streamCheckPeriod: number;
}

interface IStreamStatusInfo {
    streamer: IStreamerDto;
    status: IStreamStatusController;
}

export class StreamDiscoveryService implements IStreamDiscoveryService {
    private interval: NodeJS.Timeout | null = null;
    private readonly streams: Array<IStreamStatusInfo> = [];
    
    constructor(private config: IStreamDiscoveryServiceConfig,
                private statusControllerFactory: IStreamStatusControllerFactory,
                private twitchApi: ITwitchApi,
                private emitter: EventEmitter,
                private streamerDao: IStreamerDao) {
    }

    start() {
        if (this.interval != null) {
            return;
        }

        this.init()
            .then(
                () => this.interval = setInterval(() => this.updateServerStatus(), this.config.streamCheckPeriod)
            );
    }

    stop() {
        if (this.interval == null) {
            return;
        }
        
        while (this.streams.length) {
            this.streams.pop();
        }
        clearInterval(this.interval);
        this.interval = null;
    }

    on(state: StreamStateEvent, handler: () => void) {
        this.emitter.on(state, handler);
    }

    private emit(state: StreamStateEvent): void {
        this.emitter.emit(state);
    }

    private updateServerStatus(): void {
        const requestId = uuid.v1();
        this.streams.forEach(info => {
            this.twitchApi.isStreamOnlineByName(info.streamer.userInfo.name)
                .then(status => info.status.update(status))
                .then(newStatus => this.onStatusUpdate(requestId, newStatus, info.streamer));
        });
    }

    private onStatusUpdate(requestId: string, newStatus: NewStreamState, streamer: IStreamerDto): Promise<void> {
        switch (newStatus) {
            case NewStreamState.noChange:
                return Promise.resolve();
            case NewStreamState.goOnline:
                this.emit('online');
                return this.streamerDao.setLastOnline(requestId, streamer.id, Date.now());
            case NewStreamState.disconnected:
                this.emit('disconnect');
                return Promise.resolve();
            case NewStreamState.recovered:
                this.emit('recover');
                return Promise.resolve();
            case NewStreamState.goOffline:
                this.emit('offline');
                return this.streamerDao.setLastOffline(requestId, streamer.id, Date.now());
            default:
                throw new Error('Unsupported stream status');
        }
    }

    private init(): Promise<IStreamerDto> {
        const requestId = uuid.v1();
        return Promise.all(this.config.streamers.map(async (name) => {
            const streamer = await this.streamerDao.getStreamerByUserName(requestId, name);
            if (streamer == null) {
                throw new Error('Cannot find corresponding streamer');
            }
            // const status = await this.twitchApi.isStreamOnlineByName(name);
            this.streams.push({ streamer, status: this.statusControllerFactory.create(false) });
        })).then();
    }
}
