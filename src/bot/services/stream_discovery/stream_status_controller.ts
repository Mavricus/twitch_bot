export enum NewStreamState {
    noChange,
    goOnline,
    disconnected,
    recovered,
    goOffline
}

enum StreamState {
    online,
    disconnected,
    offline
}

export interface IStreamStatusController {
    update(status: boolean): NewStreamState;
}

interface IStreamStatusControllerConfig {
    recoveryTimeout: number;
}

export class StreamStatusController implements IStreamStatusController {
    private lastState: StreamState;
    private lastUpdateTimestamp: number = 0;

    constructor(private config: IStreamStatusControllerConfig, status: boolean) {
        this.lastState = status ? StreamState.online : StreamState.offline;
    }

    update(status: boolean): NewStreamState {
        switch (this.lastState) {
            case StreamState.online:
                return this.transferOnline(status);
            case StreamState.disconnected:
                return this.transferDisconnect(status);
            case StreamState.offline:
                return this.transferOffline(status);
            default: throw new Error('Unsupported stream state');
        }
    }

    private transferOnline(status: boolean): NewStreamState {
        if (status) {
            return NewStreamState.noChange;
        }

        this.lastState = StreamState.disconnected;
        this.lastUpdateTimestamp = Date.now();

        return NewStreamState.disconnected;
    }

    private transferDisconnect(status: boolean): NewStreamState {
        const now = Date.now();
        if (status) {
            this.lastState = StreamState.online;
            this.lastUpdateTimestamp = Date.now();
            return NewStreamState.recovered;
        }

        if (now - this.lastUpdateTimestamp < this.config.recoveryTimeout) {
            return NewStreamState.noChange;
        }

        this.lastState = StreamState.offline;
        this.lastUpdateTimestamp = now;

        return NewStreamState.goOffline;
    }

    private transferOffline(status: boolean): NewStreamState {
        if (!status) {
            return NewStreamState.noChange;
        }

        this.lastState = StreamState.online;
        this.lastUpdateTimestamp = Date.now();

        return NewStreamState.goOnline;
    }
}

export interface IStreamStatusControllerFactory {
    create(status: boolean): IStreamStatusController;
}

export class StreamStatusControllerFactory implements IStreamStatusControllerFactory {
    constructor(private config: IStreamStatusControllerConfig) {
    }

    create(status: boolean): IStreamStatusController {
        return new StreamStatusController(this.config, status);
    }
}
