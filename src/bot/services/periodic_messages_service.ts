import { Client } from 'tmi.js';
import { IAppConfig } from '../../configuration/corfig_reader';
import { ILogger } from '../../utils/logger/base_logger';

export interface IMessageConfig {
    period: number;
    text: string;
}

export class PeriodicMessageService {
    private interval: NodeJS.Timeout | null = null;
    private readonly channel: string;

    constructor(private config: IAppConfig, private logger: ILogger, private client: Client, private messageBuilder: IPeriodicMessageBuilder) {
        if (!this.config.bot.config.channels?.[0]) {
            throw new Error('Incorrect configuration. Chanel is not defined.');
        }
        this.channel = this.config.bot.config.channels[0];
    }

    start(periodSec: number): void {
        if (this.interval == null) {
            this.interval = setInterval(() => {
                this.client.say(this.channel, this.messageBuilder.build()).then();
            }, periodSec * 1000);
        }
    }

    stop(): void {
        if (this.interval != null) {
            clearInterval(this.interval);
            this.interval = null;
        }
    }
}

export interface IPeriodicMessageBuilder {
    build(): string;
}

export class PredefinedMessage implements IPeriodicMessageBuilder {
    constructor(private message: string) {
    }

    build(): string {
        return this.message;
    }
}

export class PredefinedSelfMessage implements IPeriodicMessageBuilder {
    private readonly streamer: string;

    constructor(config: IAppConfig, private message: string) {
        this.streamer = config.bot.options.streamersName;
    }

    build(): string {
        return `@${this.streamer}, ${this.message}`;
    }
}