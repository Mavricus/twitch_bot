import { IDdConnection } from './db_connection';
import { Penalties } from '../bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/moderation/moderate_types';

export interface IRudeWord {
    id: number;
    word: string;
    warningMessage?: string;
    timeoutValue: number; // in seconds
    penaltyScore: number;
    penaltyType: Penalties;
    penaltyTtl: number; // in seconds
}

export interface IRudeWordsDto {
    getWord(requestId: string, word: string): Promise<IRudeWord | null>;
    getAll(requestId: string): Promise<Array<IRudeWord>>;
}

export class RudeWordsDto implements IRudeWordsDto {
    constructor(private client: IDdConnection) {
    }
    
    getAll(requestId: string): Promise<Array<IRudeWord>> {
        return this.client.query<IRudeWord>(requestId,'SELECT ' +
                'word, ' +
                'warning_message as warningMessage, ' +
                'timeout_value as timeoutValue, ' +
                'penalty_score as penaltyScore, ' +
                'penalty_type as penaltyType, ' +
                'penalty_ttl as penaltyTtl ' +
            'FROM rude_words;', []);
    }

    getWord(requestId: string, word: string): Promise<IRudeWord | null> {
        return this.client.query<IRudeWord>(requestId, 'SELECT ' +
                'word, ' +
                'warning_message as warningMessage, ' +
                'timeout_value as timeoutValue, ' +
                'penalty_score as penaltyScore, ' +
                'penalty_type as penaltyType, ' +
                'penalty_ttl as penaltyTtl ' +
            'FROM rude_words ' +
            'WHERE word=?;', [word])
                   .then(results => results.length ? results[0] : null);
    }
}
