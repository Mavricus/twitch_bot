import { IDataStorage } from '../../utils/storages/storage_types';

export interface IStreamDao {
    startStream(streamerId: string): Promise<void>;
    stopStream(streamerId: string): Promise<void>;
    getLastStreamStartTime(streamerId: string): Promise<number>;
    getLastStreamStopTime(streamerId: string): Promise<number>;
}

export class StreamDto implements IStreamDao {
    constructor(private storage: IDataStorage) {
    }

    getLastStreamStartTime(streamerId: string): Promise<number> {
        return this.storage.get<number>(this.buildKey(streamerId, 'streamStart'))
                   .then(time => {
                       if (time != null) {
                           return Promise.resolve(time)
                       }
                       return this.startStream(streamerId).then(() => Date.now());
                   });
    }

    getLastStreamStopTime(streamerId: string): Promise<number> {
        return this.storage.get<number>(this.buildKey(streamerId, 'streamStop'))
                   .then(time => {
                       if (time != null) {
                           return Promise.resolve(time)
                       }
                       return this.stopStream(streamerId).then(() => Date.now());
                   });
    }

    startStream(streamerId: string): Promise<void> {
        return this.storage.set<number>(this.buildKey(streamerId, 'streamStart'), Date.now());
    }

    stopStream(streamerId: string): Promise<void> {
        return this.storage.set<number>(this.buildKey(streamerId, 'streamStop'), Date.now());
    }

    buildKey(streamer: string, field: string) {
        return `StreamDto:${streamer}:${field}`;
    }
}
