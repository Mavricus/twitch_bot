import { IMusic } from '../server/routes/music_get';
import { IObjectStorage } from '../utils/storages/storage_types';

export interface IMusicDao {
    get(): Promise<IMusic | null>;
    set(music: IMusic): Promise<void>;
}

export class MusicDao implements IMusicDao {
    constructor(private musicStorage: IObjectStorage<IMusic>,
                private stringStorage: IObjectStorage<string>) {
    }

    get(): Promise<IMusic | null> {
        return this.musicStorage.get();
    }

    set(music: IMusic): Promise<void> {
        return Promise.all([
            this.musicStorage.set(music),
            this.stringStorage.set(`       ...:::  ${music.artist} - ${music.track}  :::...       `)
        ]).then();
    }
}
