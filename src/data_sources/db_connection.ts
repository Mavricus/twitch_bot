import { Connection, FieldInfo, Pool } from 'mysql';
import { ILogger } from '../utils/logger/base_logger';
export const sqlMeta = Symbol('SqlMeta');

export interface ISqlMeta extends FieldInfo {
    fieldCount: number,
    affectedRows: number,
    insertId: number,
    serverStatus: number,
    warningCount: number,
    message: string,
    protocol41: boolean,
    changedRows: number
}

export type DbResult<T> = Array<T> & { [sqlMeta]: ISqlMeta };

export interface IDdConnection {
    query<T>(requestId: string, template: string, args: Array<any>): Promise<DbResult<T>>;
}

export class DdConnection implements IDdConnection {
    constructor(private connection: Connection | Pool, private logger: ILogger) {
    }

    query<T>(requestId: string, template: string, args: Array<string | number>): Promise<DbResult<T>> {
        this.logger.debug({ requestId, description: 'About to make DB request', template, args });
        return new Promise<DbResult<T>>((resolve, reject) => {
            this.connection.query(template, args, (err, data: Array<T>, meta: any) => {
                if (err) {
                    this.logger.error({ requestId, description: 'Request failed with error.', err, sql: err.sql });
                    return reject(err);
                }
                resolve(DdConnection.buildResult<T>(data, meta));
            });
        })
    }

    private static buildResult<T>(data: any, meta: ISqlMeta): DbResult<T> {
        let result: DbResult<T>;
        if (!(data instanceof Array)) {
            meta = { ...data, ...meta };
        }
        if (data == null) {
            data = [];
        }
        result = <DbResult<T>>data;
        result[sqlMeta] = meta;

        return result;
    }
}
