import { IDdConnection } from '../db_connection';

export enum UserMessageType {
    common,
    highlighted,
    custom1,
    custom2,
    custom3,
    custom4,
    custom5
}

export interface IMessageDto {
    messageId: string;
    userId: number;
    streamerId: number;
    text: string;
    type: UserMessageType;
    timestamp: number;
}

export interface IUserMessagesDao {
    getMessage(requestId: string, id: string): Promise<IMessageDto>;
    getUserMessages(requestId: string, userId: number, streamerId: number): Promise<Array<IMessageDto>>;
    getUserMessagesBetweenDates(requestId: string, userId: number, streamerId: number, from: Date, to: Date): Promise<Array<IMessageDto>>;
    addMessage(requestId: string, message: IMessageDto): Promise<void>;
}

export class UserMessagesDao implements IUserMessagesDao {
    constructor(private dbClient: IDdConnection) {
    }

    getMessage(requestId: string, id: string): Promise<IMessageDto> {
        const query = 'SELECT user_id AS userId, message_id AS messageId, streamer_id AS streamerId, text, highlighted, timestamp ' +
            'FROM user_messages ' +
            'WHERE message_id=?';
        return this.dbClient.query<IMessageDto>(requestId, query, [id])
                   .then(messages => messages[0] ?? null);
    }

    getUserMessages(requestId: string, streamerId: number, userId: number): Promise<Array<IMessageDto>> {
        const query = 'SELECT user_id AS userId, message_id AS messageId, streamer_id AS streamerId, text, highlighted, timestamp ' +
            'FROM user_messages ' +
            'WHERE user_id=?';
        return this.dbClient.query<IMessageDto>(requestId, query, [userId]);
    }

    getUserMessagesBetweenDates(requestId: string, userId: number, streamerId: number, from: Date, to: Date): Promise<Array<IMessageDto>> {
        const query = 'SELECT user_id AS userId, message_id AS messageId, streamer_id AS streamerId, text, highlighted, timestamp ' +
            'FROM user_messages ' +
            'WHERE user_id=? ' +
            'AND timestamp BETWEEN (?, ?)';
        return this.dbClient.query<IMessageDto>(requestId, query, [userId, from.getTime(), to.getTime()]);
    }

    addMessage(requestId: string, { messageId, userId, streamerId, text, type, timestamp }: IMessageDto): Promise<void> {
        const query = 'INSERT INTO user_messages (user_id, message_id, streamer_id, text, type, timestamp) VALUES (?, ?, ?, ?, ?, ?)';
        return this.dbClient.query<void>(requestId, query, [userId, messageId, streamerId, text, type, timestamp])
            .then();
    }
}
