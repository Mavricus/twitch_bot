import { IDdConnection, sqlMeta } from '../db_connection';
import { IUserDto, IUsersDao } from './user_dao';

export interface IStreamerInfo {
    userId: number;
}

interface IStreamerDetails extends IStreamerInfo {
    id: number;
    lastOnline: number | null;
    lastOffline: number | null;
}

export interface IStreamerDto extends IStreamerDetails {
    userInfo: IUserDto;
}

const QUERIES = {
    searchByUserId: 'SELECT id, user_id AS userId, lastOnlineTimestamp AS lastOnline, lastOfflineTimestamp AS lastOffline FROM streamers WHERE user_id=?',
    searchById: 'SELECT id, user_id AS userId, lastOnlineTimestamp AS lastOnline, lastOfflineTimestamp AS lastOffline FROM streamers WHERE id=?',
    addStreamer: 'INSERT INTO streamers (user_id) VALUE ?',
    updateOffline: 'UPDATE streamers SET lastOfflineTimestamp=? WHERE id=?',
    updateOnline: 'UPDATE streamers SET lastOnlineTimestamp=? WHERE id=?'
}

export interface IStreamerDao {
    addStreamer(requestId: string, streamer: IStreamerInfo): Promise<IStreamerDto>;
    setLastOnline(requestId: string, streamerId: number, timestamp: number): Promise<void>;
    setLastOffline(requestId: string, streamerId: number, timestamp: number): Promise<void>;
    getStreamer(requestId: string, streamerId: number): Promise<IStreamerDto | null>;
    getStreamerByUserId(requestId: string, userId: number): Promise<IStreamerDto | null>;
    getStreamerByUserName(requestId: string, userName: string): Promise<IStreamerDto | null>;
    isStreamer(requestId: string, userId: number): Promise<boolean>;
}

export class StreamersDao implements IStreamerDao {
    constructor(private dbClient: IDdConnection, private userDao: IUsersDao) {
    }

    addStreamer(requestId: string, { userId }: IStreamerInfo): Promise<IStreamerDto> {
        return this.dbClient.query<void>(requestId, QUERIES.addStreamer, [userId])
                   .then(result => result[sqlMeta].insertId)
                   .then(id => this.buildStreamerObject(requestId, { userId, id, lastOffline: null, lastOnline: null }));
    }

    getStreamer(requestId: string, streamerId: number): Promise<IStreamerDto | null> {
        return this.dbClient.query<IStreamerDetails>(requestId, QUERIES.searchById, [streamerId])
                   .then(streamers => this.buildSearchResult(requestId, streamers[0]));
    }

    getStreamerByUserId(requestId: string, userId: number): Promise<IStreamerDto | null> {
        return this.dbClient.query<IStreamerDetails>(requestId, QUERIES.searchByUserId, [userId])
                   .then(streamers => this.buildSearchResult(requestId, streamers[0]));
    }

    isStreamer(requestId: string, userId: number): Promise<boolean> {
        return this.getStreamerByUserId(requestId, userId)
                   .then(streamer => streamer != null);
    }

    setLastOffline(requestId: string, streamerId: number, timestamp: number): Promise<void> {
        return this.dbClient.query<void>(requestId, QUERIES.updateOffline, [timestamp, streamerId])
                   .then();
    }

    setLastOnline(requestId: string, streamerId: number, timestamp: number): Promise<void> {
        return this.dbClient.query<void>(requestId, QUERIES.updateOnline, [timestamp, streamerId])
                   .then();
    }

    getStreamerByUserName(requestId: string, userName: string): Promise<IStreamerDto | null> {
        return this.userDao.getUserByTwitchName(requestId, userName)
                   .then(user => {
                       if (user?.id == null) {
                           return Promise.resolve(null);
                       }
                       return this.getStreamerByUserId(requestId, user.id);
                   });
    }

    private buildSearchResult(requestId: string, streamer: IStreamerDetails): Promise<IStreamerDto | null> {
        if (streamer == null) {
            return Promise.resolve(null);
        }
        return this.buildStreamerObject(requestId, streamer);
    }

    private buildStreamerObject(requestId: string, streamer: IStreamerDetails): Promise<IStreamerDto> {
        return this.userDao.getUserById(requestId, streamer.userId)
                   .then(userInfo => {
                       if (userInfo == null) {
                           throw new Error("Cannot find streamer's userInformation");
                       }
                       
                       return Object.assign(streamer, { userInfo });
                   });
    }
}
