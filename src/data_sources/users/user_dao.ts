import { IDdConnection, sqlMeta } from '../db_connection';

export interface IUserInfo {
    id?: number;
    name: string;
    twitch_id: string;
}

export interface IUserDto extends IUserInfo {
    aliases: Array<string>;
}

export interface IUsersDao {
    //TODO: Check streamerId as well.
    getUserByTwitchName(requestId: string, name: string): Promise<IUserDto | null>;
    getUserById(requestId: string, id: number): Promise<IUserDto | null>;
    getUserByTwitchId(requestId: string, id: string): Promise<IUserDto | null>;
    addUser(requestId: string, info: IUserInfo): Promise<IUserDto>;
    addAlias(requestId: string, alias: string, userId: number, streamerId: number): Promise<IUserDto>;
    delete(requestId: string, userId: number): Promise<void>;
    update(requestId: string, user: IUserInfo): Promise<IUserDto>;
}

export class UserDao implements IUsersDao {
    constructor(private dbClient: IDdConnection) {
    }

    getUserById(requestId: string, id: number): Promise<IUserDto | null> {
        return this.dbClient.query<IUserInfo>(requestId, 'SELECT * FROM users AS u WHERE u.id=?;', [id])
            .then(userInfo => this.getUserDetailsDto(requestId, userInfo));
    }

    getUserByTwitchName(requestId: string, name: string): Promise<IUserDto | null> {
        return this.dbClient.query<IUserInfo>(requestId, 'SELECT * FROM users AS u WHERE u.name=?;', [name])
                   .then(result => this.getUserDetailsDto(requestId, result));
    }

    getUserByTwitchId(requestId: string, id: string): Promise<IUserDto | null> {
        return this.dbClient.query<IUserInfo>(requestId, 'SELECT * FROM users AS u WHERE u.twitch_id=?;', [id])
                   .then(result => this.getUserDetailsDto(requestId, result));
    }

    async addAlias(requestId: string, alias: string, userId: number, streamerId: number): Promise<IUserDto> {
        const query = 'INSERT INTO user_aliases (user_id, alias, streamer_id) VALUES (?, ?, ?);';
        return this.dbClient.query<void>(requestId, query, [userId, alias, streamerId])
                   .then(() => this.getUserById(requestId, userId))
                   .then(user => {
                       if (user == null) {
                           return Promise.reject(new Error('Adding of the user is failed, please try again.'));
                       }
                       return user;
                   });
    }

    addUser(requestId: string, { name, twitch_id }: IUserInfo): Promise<IUserDto> {
        return this.dbClient.query<void>(requestId, 'INSERT INTO users (name, twitch_id) VALUES (?, ?);', [name, twitch_id])
                   .then(result => result[sqlMeta].insertId)
                   .then(id => this.getUserById(requestId, id))
                   .then(user => {
                       if (user == null) {
                           return Promise.reject(new Error('Adding of the user is failed, please try again.'));
                       }
                       return user;
                   });
    }

    delete(requestId: string, userId: number): Promise<void> {
        return this.dbClient.query<void>(requestId, 'DELETE FROM users WHERE id=?', [userId]).then();
    }

    update(requestId: string, user: IUserInfo): Promise<IUserDto> {
        if (user.id == null) {
            throw new Error('User ID is not specified');
        }
        const userId = user.id;
        const args: { names: Array<string>, values: Array<any> } = { names: [], values: [] };
        if (user.name != null) {
            args.names.push('name');
            args.values.push(user.name);
        }
        if (user.twitch_id != null) {
            args.names.push('twitch_id');
            args.values.push(user.twitch_id);
        }
        args.values.push(userId);
        
        return this.dbClient.query<void>(requestId, `UPDATE users SET ${args.names.map(name => name + '=?').join(', ')} WHERE id=?`, args.values)
                   .then(() => this.getUserById(requestId, userId))
                   .then(user => {
                       if (user == null) {
                           throw new Error('User information cannot be retrieved');
                       }
                       return user;
                   });
    }

    private getUserDetailsDto(requestId: string, users: Array<IUserInfo>): Promise<IUserDto | null> {
        if (!users.length) {
            return Promise.resolve(null);
        }
        if (users.length > 1) {
            return Promise.reject(new Error('More then one user was find at the DB.'));
        }
        const [user] = users;

        return this.dbClient.query<{ alias: string }>(requestId, 'SELECT alias FROM user_aliases WHERE user_id=?', [user.id])
                   .then(aliases => UserDao.buildUserDto(user, aliases));
    }

    private static buildUserDto(user: IUserInfo, aliases: Array<{ alias: string }>): IUserDto {
        const aliasesList = aliases.map(item => item.alias);
        
        return Object.assign({}, user, { aliases: aliasesList });
    }
}
