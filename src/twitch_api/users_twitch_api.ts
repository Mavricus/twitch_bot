import twitchApi from 'twitch-api-v5';
import { promisify } from 'util';

export interface ITwitchUser {
    displayName: string;
    id: number;
    name: string;
    type: string;
    bio: string;
    createdAt: Date;
    updatedAt: Date;
    logoUrl: string;
}


interface IUserData {
    "display_name": string,
    "_id": string,
    "name": string,
    "type": string,
    "bio": string,
    "created_at": string,
    "updated_at": string,
    "logo": string;
}

export interface ITwitchUsersApi {
    getUserById(id: number): Promise<ITwitchUser | null>;
    getUserByName(name: string): Promise<ITwitchUser | null>;
}

export class TwitchUsersApi implements ITwitchUsersApi {
    constructor(private client: typeof twitchApi) {
    }

    async getUserById(id: number): Promise<ITwitchUser | null> {
        let user = await promisify(this.client.users.userByID)({ userID: '' + id });
        if (!user) {
            return null;
        }
        return TwitchUsersApi.convertData(user);
    }

    async getUserByName(name: string): Promise<ITwitchUser | null> {
        const userName = name.toLowerCase();
        let data: { users: Array<IUserData> } = await promisify(this.client.users.usersByName)({ users: name });
        if (!data?.users?.length) {
            return null;
        }
        let user = data.users.find(({ name }) => name.toLowerCase() === userName);
        if (!user) {
            return null;
        }
        return TwitchUsersApi.convertData(user);
    }

    private static convertData(user: IUserData): ITwitchUser {
        return {
            id: +user._id,
            name: user.name,
            displayName: user.display_name,
            type: user.type,
            bio: user.bio,
            logoUrl: user.logo,
            createdAt: new Date(user.created_at),
            updatedAt: new Date(user.updated_at)
        };
    }
}