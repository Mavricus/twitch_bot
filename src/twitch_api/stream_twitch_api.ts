import twitchApi from 'twitch-api-v5';
import { promisify } from 'util';
import { IChannelData } from './chanel_twitch_api';

export interface ITwitchStream {
    id: number;
    game: string;
    broadcastPlatform: string;
    communityId: string;
    communityIds: Array<string>;
    viewers: number;
    videoHeight: number;
    averageFps: number;
    delay: number;
    createdAt: Date;
    isPlaylist: boolean;
    streamType: string;
    preview: {
        smallUrl: string;
        mediumUrl: string;
        largeUrl: string;
        templateUrl: string;
    };
}

interface IStreamPreviews {
    small: string;
    medium: string;
    large: string;
    template: string;
}

interface IStreamDetails {
    _id: number;
    game: string;
    broadcast_platform: string;
    community_id: string;
    community_ids: Array<string>;
    viewers: number;
    video_height: number;
    average_fps: number;
    delay: number;
    created_at: string;
    is_playlist: boolean;
    stream_type: string;
    preview: IStreamPreviews;
    channel: IChannelData;
}

interface IStreamData {
    stream: IStreamDetails;
}

export interface ITwitchStreamApi {
    getStreamById(id: number): Promise<ITwitchStream | null>;
}

export class TwitchStreamApi implements ITwitchStreamApi {
    constructor(private client: typeof twitchApi) {
    }

    async getStreamById(id: number): Promise<ITwitchStream | null> {
        let data: IStreamData = await promisify(this.client.streams.channel)({ channelID: '' + id, stream_type: 'all' });
        if (!data?.stream) {
            return null;
        }
        return TwitchStreamApi.convert(data);
    }

    private static convert({ stream }: IStreamData): ITwitchStream {
        return {
            id: stream._id,
            game: stream.game,
            delay: stream.delay,
            averageFps: stream.average_fps,
            broadcastPlatform: stream.broadcast_platform,
            communityId: stream.community_id,
            communityIds: stream.community_ids,
            createdAt: new Date(stream.created_at),
            isPlaylist: stream.is_playlist,
            preview: {
                largeUrl: stream.preview.large,
                mediumUrl: stream.preview.medium,
                smallUrl: stream.preview.small,
                templateUrl: stream.preview.template
            },
            streamType: stream.stream_type,
            videoHeight: stream.video_height,
            viewers: stream.viewers
        };
    }
}