import { ITwitchUser, ITwitchUsersApi } from './users_twitch_api';
import { ITwitchChannel, ITwitchChannelApi } from './chanel_twitch_api';
import { ITwitchStream, ITwitchStreamApi } from './stream_twitch_api';

export interface ITwitchApi extends ITwitchUsersApi, ITwitchChannelApi, ITwitchStreamApi {
    getChannelByUserName(name: string): Promise<ITwitchChannel | null>;
    getStreamByUserName(name: string): Promise<ITwitchStream| null>;
    isStreamOnlineByName(name: string): Promise<boolean>;
    isStreamOnlineById(id: number): Promise<boolean>;
}

export class TwitchApi implements ITwitchApi {
    constructor(private userApi: ITwitchUsersApi,
                private channelApi: ITwitchChannelApi,
                private streamApi: ITwitchStreamApi) {
    }

    async getChannelByUserName(name: string): Promise<ITwitchChannel | null> {
        let userInfo = await this.getUserByName(name);
        if (!userInfo) {
            return null;
        }
        return this.getChannelById(userInfo.id);
    }

    async getStreamByUserName(name: string): Promise<ITwitchStream | null> {
        const chanel = await this.getChannelByUserName(name);
        if (chanel == null) {
            return null;
        }
        return this.getStreamById(chanel.id);
    }

    async isStreamOnlineByName(name: string): Promise<boolean> {
        return TwitchApi.isStreamOnline(await this.getStreamByUserName(name));
    }

    async isStreamOnlineById(id: number): Promise<boolean> {
        return TwitchApi.isStreamOnline(await this.getStreamById(id));
    }

    getUserById(id: number): Promise<ITwitchUser | null> {
        return this.userApi.getUserById(id);
    }

    getUserByName(name: string): Promise<ITwitchUser | null> {
        return this.userApi.getUserByName(name);
    }

    getChannelById(id: number): Promise<ITwitchChannel | null> {
        return this.channelApi.getChannelById(id);
    }

    getStreamById(id: number): Promise<ITwitchStream | null> {
        return this.streamApi.getStreamById(id);
    }

    private static isStreamOnline(stream: ITwitchStream | null): boolean {
        if (stream == null) {
            return false;
        }
        // return stream.streamType === 'live' && !stream.isPlaylist && stream.broadcastPlatform === 'live';
        return stream.streamType === 'live';
    }
}