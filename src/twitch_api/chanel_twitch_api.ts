import { promisify } from 'util';
import twitchApi from 'twitch-api-v5';

export interface ITwitchChannel {
    id: number;
    status: string;
    game: string;
    mature: boolean;
    broadcasterLanguage: string;
    broadcasterSoftware: string;
    displayName: string;
    language: string;
    name: string;
    createdAt: Date;
    updatedAt: Date;
    partner: boolean;
    logoUrl: string;
    videoBannerUrl: string;
    profileBannerUrl: string;
    profileBannerBackgroundColor: string;
    channelUrl: string;
    views: number;
    followers: number;
    broadcasterType: string;
    description: string;
    privateVideo: boolean;
    privacyOptionsEnabled: boolean;
}

export interface IChannelData {
    mature: boolean;
    status: string;
    broadcaster_language: string;
    broadcaster_software: string;
    display_name: string;
    game: string;
    language: string;
    _id: string;
    name: string;
    created_at: string;
    updated_at: string;
    partner: boolean;
    logo: string;
    video_banner: string;
    profile_banner: string;
    profile_banner_background_color: string;
    url: string;
    views: number;
    followers: number;
    broadcaster_type: string;
    description: string;
    private_video: boolean;
    privacy_options_enabled: boolean;
}

export interface ITwitchChannelApi {
    getChannelById(id: number): Promise<ITwitchChannel | null>
}

export class TwitchChannelApi implements ITwitchChannelApi {
    constructor(private client: typeof twitchApi) {
    }

    async getChannelById(id: number): Promise<ITwitchChannel | null> {
        let data: IChannelData = await promisify(this.client.channels.channelByID)({ channelID: '' + id });
        if (!data) {
            return  null;
        }
        return TwitchChannelApi.convertData(data);
    }

    private static convertData(data: IChannelData): ITwitchChannel {
        return {
            id: +data._id,
            status: data.status,
            game: data.game,
            broadcasterLanguage: data.broadcaster_language,
            broadcasterSoftware: data.broadcaster_software,
            broadcasterType: data.broadcaster_type,
            channelUrl: data.url,
            createdAt: new Date(data.created_at),
            description: data.description,
            displayName: data.display_name,
            followers: data.followers,
            language:data.language,
            logoUrl: data.logo,
            mature: data.mature,
            name: data.name,
            partner: data.partner,
            privacyOptionsEnabled: data.privacy_options_enabled,
            privateVideo: data.private_video,
            profileBannerBackgroundColor: data.profile_banner_background_color,
            profileBannerUrl: data.profile_banner,
            updatedAt: new Date(data.updated_at),
            videoBannerUrl: data.video_banner,
            views: data.views
        };
    }
}