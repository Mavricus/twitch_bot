import { getLocalizedNumber } from './collections';

const ONE_SECOND = 1000;
const ONE_MINUTE = 60 * ONE_SECOND;
const ONE_HOUR = 60 * ONE_MINUTE;
const ONE_DAY = 24 * ONE_HOUR;

export const getLocalizedHoursString = (h: number) => {
    return getLocalizedNumber(h, ['час', 'часа', 'часов']);
};

export const getLocalizedMinutesString = (m: number): string => {
    return getLocalizedNumber(m, ['минуту', 'минуты', 'минут']);
};

export const getLocalizedSecondsString = (s: number): string => {
    return getLocalizedNumber(s, ['секунду', 'секунды', 'секунд']);
};

export const getLocalizedDaysString = (d: number): string => {
    return getLocalizedNumber(d, ['день', 'дня', 'дней']);
};

export const getLocalizedTime = (time: number): string => {
    const days = Math.floor(time / ONE_DAY);
    const hours = Math.floor((time % ONE_DAY) / ONE_HOUR);
    const minutes = Math.floor((time % ONE_HOUR) / ONE_MINUTE);
    const seconds = Math.floor((time % ONE_MINUTE) / ONE_SECOND);

    if (days > 0) {
        return hours === 0 ? getLocalizedDaysString(days) : `${getLocalizedDaysString(days)} и ${getLocalizedHoursString(hours)}`;
    }
    if (hours > 0 ) {
        return minutes === 0 ? getLocalizedHoursString(hours) : `${getLocalizedHoursString(hours)} ${getLocalizedMinutesString(minutes)}`;
    }
    return seconds === 0 ? getLocalizedSecondsString(minutes) : `${getLocalizedMinutesString(minutes)} ${getLocalizedSecondsString(seconds)}`;
};
