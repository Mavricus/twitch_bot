import { IDataStorage } from './storage_types';

export class InMemoryStorage implements IDataStorage {
    private storage: { [key: string]: any } = { };

    get<T>(name: string): Promise<T> {
        return Promise.resolve(this.storage[name]);
    }

    set<T>(name: string, data: T): Promise<void> {
        return Promise.resolve(this.storage[name] = data).then();
    }

    delete(name: string): Promise<void> {
        return Promise.resolve(() => delete this.storage[name]).then();
    }
}