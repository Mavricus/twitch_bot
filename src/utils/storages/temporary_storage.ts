import { ITypedDataStorage } from './storage_types';

export abstract class InMemoryTemporaryStorage<T> implements ITypedDataStorage<T> {
    private storage: Set<T> = new Set<T>();

    getAll(): Promise<Array<T> | null> {
        return Promise.resolve(Array.from(this.storage));
    }

    add(data: T): Promise<void> {
        setTimeout(() => this.storage.delete(data), this.getExpirationTime(data));
        this.storage.add(data);
        return Promise.resolve();
    }
    
    protected abstract getExpirationTime(data: T): number;
}
