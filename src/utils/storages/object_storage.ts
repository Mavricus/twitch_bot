import { IDataStorage, IObjectStorage } from './storage_types';

export class ObjectStorage<T> implements IObjectStorage<T> {
    constructor(private storage: IDataStorage, private name: string) {
    }

    get(): Promise<T | null> {
        return this.storage.get(this.buildKey());
    }

    set(data: T): Promise<void> {
        return this.storage.set(this.buildKey(), data);
    }

    delete(): Promise<void> {
        return this.storage.delete(this.buildKey());
    }

    private buildKey(): string {
        return `objectStorage:${this.name}`;
    }
}
