export interface IDataStorage {
    get<T>(name: string): Promise<T | null>;
    set<T>(name: string, data: T): Promise<void>;
    delete(name: string): Promise<void>;
}

export interface ITypedDataStorage<T> {
    getAll(): Promise<Array<T> | null>;
    add(data: T): Promise<void>;
}

export interface ITypedDataStorageFactory<T> {
    create(): ITypedDataStorage<T>;
}

export interface IObjectStorage<T> {
    get(): Promise<T | null>;
    set(data: T): Promise<void>;
    delete(): Promise<void>;
}