import { readFile, writeFile } from 'fs';
import { promisify } from 'util';
import { IDataStorage, IObjectStorage } from './storage_types';

interface IFs {
    readFile: typeof readFile;
    writeFile: typeof writeFile;
}

export class FsStringStorage implements IObjectStorage<string> {
    constructor(private path: string, private fs: IFs) {
    }

    get(): Promise<string> {
        return promisify(this.fs.readFile)(this.path, 'utf-8');
    }

    set(data: string): Promise<void> {
        return promisify(this.fs.writeFile)(this.path, data);
    }

    delete(): Promise<void> {
        return this.set('');
    }
}

export class FileStorage implements IDataStorage {
    private cache: { [key: string]: any } | null = null;
    
    constructor(private path: string, private  fs: IFs) {
    }

    async delete(name: string): Promise<void> {
        if (this.cache == null) {
            this.cache = await this.updateCache();
        }
        delete this.cache[name];
        return promisify(this.fs.writeFile)(this.path, JSON.stringify(this.cache));
    }

    async get<T>(name: string): Promise<T | null> {
        if (this.cache == null) {
            this.cache = await this.updateCache();
        }
        return this.cache[name];
    }

    async set<T>(name: string, data: T): Promise<void> {
        if (this.cache == null) {
            this.cache = await this.updateCache();
        }
        this.cache[name] = data;
        return promisify(this.fs.writeFile)(this.path, JSON.stringify(this.cache));
    }

    private async updateCache(): Promise<{ [key: string]: any }> {
        return JSON.parse(await promisify(this.fs.readFile)(this.path, 'utf-8'));
    }
}