export interface ILogMessage {
    description: string;
    requestId: string;
    err?: Error;
}

export interface ILogger {
    trace<T extends ILogMessage>(message: T): void;
    debug<T extends ILogMessage>(message: T): void;
    info<T extends ILogMessage>(message: T): void;
    warn<T extends ILogMessage>(message: T): void;
    error<T extends ILogMessage>(message: T): void;
    emergency<T extends ILogMessage>(message: T): void;
    log<T extends ILogMessage>(logLevel: LogLevels, message: T): void;
    setLoglevel(level: string): ILogger;
}

export enum LogLevels {
    trace,
    debug,
    info,
    warn,
    error,
    emergency,
}

export interface IMessageScope<T extends ILogMessage> {
    dateTime: string;
    loglevel: string;
    requestId: string;
    errorDetails?: string;
    data: T;
}

export abstract class BaseLogger implements ILogger {
    private logLevel = LogLevels.debug;

    debug<T extends ILogMessage>(message: T) {
        this.log(LogLevels.debug, message);
    }

    emergency<T extends ILogMessage>(message: T) {
        this.log(LogLevels.emergency, message);
    }

    error<T extends ILogMessage>(message: T) {
        this.log(LogLevels.error, message);
    }

    info<T extends ILogMessage>(message: T) {
        this.log(LogLevels.info, message);
    }

    trace<T extends ILogMessage>(message: T) {
        this.log(LogLevels.trace, message);
    }

    warn<T extends ILogMessage>(message: T) {
        this.log(LogLevels.warn, message);
    }

    setLoglevel(level: keyof typeof LogLevels): ILogger {
        this.logLevel = LogLevels[level];
        return this;
    }

    public log<T extends ILogMessage>(logLevel: LogLevels, message: T) {
        if (logLevel < this.logLevel) {
            return;
        }

        const scope = BaseLogger.buildMessage(logLevel, message);

        this.processMessage(logLevel, scope);
    }

    private static buildMessage<T extends ILogMessage>(logLevel: LogLevels, message: T): IMessageScope<T> {
        let scope: IMessageScope<T> = {
            dateTime: new Date().toISOString(),
            loglevel: LogLevels[logLevel],
            data: Object.assign({}, message),
            requestId: message.requestId
        };
        delete scope.data.requestId;

        if (message.err != null) {
            scope.errorDetails = message.err.stack || message.err.message;
            delete scope.data.err;
        }

        return scope;
    }

    protected abstract processMessage<T extends ILogMessage>(logLevel: LogLevels, scope: IMessageScope<T>): void;
}