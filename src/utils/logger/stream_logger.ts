import { BaseLogger, ILogMessage, IMessageScope, LogLevels } from './base_logger';
import { text } from 'express';

export class StreamLogger extends BaseLogger {
    constructor(private logStream: NodeJS.WriteStream, private errorStream: NodeJS.WriteStream) {
        super();
    }

    protected processMessage<T extends ILogMessage>(logLevel: LogLevels, scope: IMessageScope<T>): void {
        const text = JSON.stringify(scope);
        
        if (logLevel < LogLevels.error) {
            this.logStream.write(text);
        } else {
            this.errorStream.write(text);
        }
    }
}