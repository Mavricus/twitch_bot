import { BaseLogger, ILogMessage, IMessageScope, LogLevels } from './base_logger';

interface IConsole {
    log: typeof console.log;
    error: typeof console.error;
}

export class ConsoleLogger extends BaseLogger {
    constructor(private console: IConsole) {
        super();
    }

    protected processMessage<T extends ILogMessage>(logLevel: LogLevels, scope: IMessageScope<T>): void {
        const text = JSON.stringify(scope);

        if (logLevel < LogLevels.error) {
            this.console.log(text);
        } else {
            this.console.error(text);
        }
    }
}