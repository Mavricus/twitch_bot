export function randomInt(spread: number = 100, offset: number = 0): number {
    let value = Math.round(Math.random() * spread);
    value = value === spread ? 0 : value;
    
    return value + offset;
}
