import { IDataStorage } from './storages/storage_types';
import uuid from 'uuid';

export const getLocalizedNumber = (value: number, [one, two, many]: [string, string, string]): string => {
    let hundreds = value % 100;
    if (10 < hundreds && hundreds < 15) {
        return `${value} ${many}`;
    }
    switch (hundreds % 10) {
        case 1: return `${value} ${one}`;
        case 2:
        case 3:
        case 4: return `${value} ${two}`;
        default: return `${value} ${many}`;
    }
};

export interface IDictionary extends IDataStorage {
    getKeys(): Promise<Array<string>>;
}

export class Dictionary implements IDictionary {
    private keys: Set<string> = new Set<string>();

    constructor(private storage: IDataStorage, private prefix: string) {
    }

    get<T>(name: string): Promise<T | null> {
        return this.storage.get<T>(this.buildKey(name));
    }

    set<T>(name: string, data: T): Promise<void> {
        this.keys.add(name);
        return this.storage.set(this.buildKey(name), data);
    }
    delete(name: string): Promise<void> {
        this.keys.delete(name);
        return this.storage.delete(name);
    }

    async getKeys(): Promise<Array<string>> {
        return Array.from(this.keys);
    }

    private buildKey(name: string): string {
        return `dictionary:${this.prefix}:${name}`;
    }
}

export interface IDictionaryFactory {
    create(): IDictionary;
}

export class DictionaryFactory implements IDictionaryFactory {
    constructor(private storage: IDataStorage, private prefix: string) {
    }

    create(): IDictionary {
        return new Dictionary(this.storage, this.prefix + uuid.v1());
    }

}