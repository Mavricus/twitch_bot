import { Options } from 'tmi.js';
import { LogLevels } from '../utils/logger/base_logger';
import uuid from 'uuid';

interface IDbConfig {
    connectionLimit: number,
    host: string;
    port: number;
    user: string;
    password: string;
    database: string;
}

export interface IAppConfig {
    bot: {
        config: Options;
        options: {
            followersExpected: number;
            followersFile: string;
            musicFile: string;
            streamersName: string;
            twitchApiClientId: string;
        };
    };
    spotify: {
        clientId: string;
        clientSecret: string;
        redirectUri: string;
        accessToken: string;
        refreshToken: string;
    };
    server: {
        host: string;
        port: number;
    };
    db: {
        users: IDbConfig;
        moderation: IDbConfig;
    };
    logger: {
        level: keyof typeof LogLevels;
    };
    systemRequestId: string;
}

export interface IAppConfigReader {
    forceUpdate(): IAppConfig;
    get(): IAppConfig;
}

export class FsJsonConfigReader implements IAppConfigReader {
    private config: IAppConfig | null = null;

    constructor(private configPath: string) {
    }

    forceUpdate(): IAppConfig {
        try {
            const newConfig = require(this.configPath);
            return this.config = FsJsonConfigReader.mergeConfig(newConfig);
        } catch (e) {
            throw new Error('Cannot update application config');
        }
    }

    get(): IAppConfig {
        if (this.config) {
            return this.config;
        }
        return this.forceUpdate();
    }

    private static mergeConfig(newConfig: Partial<IAppConfig>): IAppConfig {
        return {
            bot: {
                config: {
                    channels: newConfig.bot?.config?.channels ?? [],
                    connection: {
                        reconnect: newConfig.bot?.config?.connection?.reconnect ?? true,
                        secure: newConfig.bot?.config?.connection?.secure ?? true
                    },
                    identity: {
                        username: newConfig.bot?.config?.identity?.username ?? '',
                        password: newConfig.bot?.config?.identity?.password ?? ''
                    },
                    options: {
                        debug: newConfig.bot?.config?.options?.debug ?? false
                    }
                },
                options: {
                    musicFile: newConfig.bot?.options?.musicFile ?? './music.txt',
                    streamersName: newConfig.bot?.options?.streamersName ?? 'tester',
                    twitchApiClientId:  newConfig.bot?.options?.twitchApiClientId ??'Twitch Client ID',
                    followersFile: newConfig.bot?.options?.followersFile ?? './followers.txt',
                    followersExpected: newConfig.bot?.options?.followersExpected ?? 50
                }
            },
            spotify: {
                clientId: newConfig.spotify?.clientId ?? 'Spotify client ID',
                clientSecret: newConfig.spotify?.clientSecret ?? 'Spotify client ID',
                redirectUri: newConfig.spotify?.redirectUri ?? 'Spotify client ID',
                accessToken: newConfig.spotify?.accessToken ?? 'Spotify access token',
                refreshToken: newConfig.spotify?.refreshToken ?? 'Spotify refresh token'
            },
            server: {
                host: newConfig.server?.host ?? '127.0.0.1',
                port: newConfig.server?.port ?? 3030
            },
            db: {
                users: {
                    connectionLimit: newConfig.db?.users?.connectionLimit ?? 20,
                    host: newConfig.db?.users?.host ?? '127.0.0.1',
                    password: newConfig.db?.users?.password ?? '<password>',
                    user: newConfig.db?.users?.user ?? 'bot',
                    port: newConfig.db?.users?.port ?? 3306,
                    database: newConfig.db?.users?.database ?? 'TwitchBot'
                },
                moderation: {
                    connectionLimit: newConfig.db?.moderation?.connectionLimit ?? 20,
                    host: newConfig.db?.moderation?.host ?? '127.0.0.1',
                    password: newConfig.db?.moderation?.password ?? '<password>',
                    user: newConfig.db?.moderation?.user ?? 'bot',
                    port: newConfig.db?.moderation?.port ?? 3306,
                    database: newConfig.db?.moderation?.database ?? 'TwitchBot'
                },
            },
            logger: {
                level: newConfig.logger?.level ?? 'debug'
            },
            systemRequestId: uuid.v1()
        };
    }
}
