import { Router } from 'express';
import { IMusicHandlerFactory } from './routes/music_get';
import { IProxyRouter } from './routes/proxy_router';

export interface IServerRouterFactory {
    create(): Router;
}

export class ServerRouterFactory implements IServerRouterFactory {
    constructor(private musicFactory: IMusicHandlerFactory,
                private proxy: IProxyRouter) {
    }

    create(): Router {
        let router = Router();
        
        router.all('*', this.proxy.createAllHandler());

        router.get('/music', this.musicFactory.createGetHandler());
        router.post('/music', this.musicFactory.createPostHandler());

        return router;
    }
}