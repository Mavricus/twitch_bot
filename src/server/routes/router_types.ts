import { RequestHandler } from 'express';

export interface IGetHandlerFactory {
    createGetHandler(): RequestHandler;
}

export interface IPostHandlerFactory {
    createPostHandler(): RequestHandler;
}

export interface IAllHandlerFactory {
    createAllHandler(): RequestHandler;
}