import { IGetHandlerFactory, IPostHandlerFactory } from './router_types';
import { RequestHandler } from 'express';
import { ILogger } from '../../utils/logger/base_logger';
import { IMusicDao } from '../../data_sources/music_dao';

export interface IMusic {
    artist: string;
    track: string;
}

export interface IMusicHandlerFactory extends IGetHandlerFactory, IPostHandlerFactory {
}

export class MusicHandlerFactory implements IMusicHandlerFactory {
    constructor(private musicStorage: IMusicDao,
                private logger: ILogger) {
    }

    createGetHandler(): RequestHandler {
        return (req, resp) => {
            this.musicStorage.get().then(data => resp.send(JSON.stringify(data)));
        };
    }

    createPostHandler(): RequestHandler {
        return (req, resp) => {
            let requestId = req.params.requestId;
            let music: IMusic = req.body;
            this.musicStorage.set(music)
                .then(() => {
                    resp.statusCode = 204;
                    resp.send();
                }).catch((err) => {
                    this.logger.error({ description: 'Cannot handle server request', requestId, err });
                    resp.statusCode = 500;
                    resp.send();
                });
        };
    }
}

