import { IAllHandlerFactory } from './router_types';
import { RequestHandler } from 'express';
import uuid from 'uuid';
import { ILogger } from '../../utils/logger/base_logger';

export interface IProxyRouter extends IAllHandlerFactory {

}

export class ProxyRouter implements IAllHandlerFactory {
    constructor(private logger: ILogger) {
    }

    createAllHandler(): RequestHandler {
        return (req, rest, next) => {
            if (req.params.requestId != null) {
                req.params.requestId = uuid.v1();
            }
            let { params, path, body } = req;
            let requestId = params.requestId;
            this.logger.info( { description: 'New server request', requestId,  path, params, body });
            return next();
        };
    }
}