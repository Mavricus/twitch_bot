import { IAppConfig } from '../configuration/corfig_reader';
import { Express } from 'express';
import { IServerRouterFactory } from './server_router';
import { promisify } from 'util';
import cors from 'cors';
import express from 'express';

export class AppServerFactory {
    constructor(private routerFactory: IServerRouterFactory) {
    }

    async startServer({ server }: IAppConfig): Promise<Express> {
        let app: Express = express();

        app.use(cors());
        app.use(express.json());
        app.use(express.urlencoded({ extended: true }));

        app.use(this.routerFactory.create());

        await promisify((callback) => app.listen(server.port, server.host, callback))();

        return app;
    }
}
