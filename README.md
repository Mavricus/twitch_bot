Twitch Bot 
==========

Overview
--------
This is a twitch bot application, which allows moderate chat and perform some actions on user's demand.

Application Configuration
-------------------------    
Application requires the configuration file `./config.json`. Config structure:
````typescript
interface IAppConfig {
    bot: {
        config: {
            channels: Array<string>;
            connection: {
                reconnect: boolean;
                secure: boolean;
            };
            identity: {
                username: string;
                password: string;
            };
            options: {
                debug: boolean;
            };
        };
        options: {
            musicFile: string;
            rudeWordsFile: string;
            streamersName: string;
            twitchApiClientId: string;
        };
    };
    server: {
        host: string;
        port: number;
    };
    logger: {
        level: 'trace' | 'debug' | 'info' | 'warn' | 'error' | 'emergency';
    };
} 
````            

Path to the configuration file can be specified at the application parameter. For example:

    node ./main.js --config ~/myAppConfig.json 

Rude Words
----------
Bot supports a blacklisting of specific words. All words can be listed at the configuration file. Path to the file can be specified at the application configuration `bot.options.rudeWordsFile`.  
Rude words file interface:

````typescript
type RudeWordsConfig = Array<{
    words: Array<string>,
    warningMessage: string,
    timeoutValue: number,  // In seconds
    penaltyScore: number,  // 1000 points leads to timeout 
    penaltyType: 'none' | 'verbal' | 'timeout' | 'ban',
    penaltyTtl: number // In seconds 
}>
````

Application Start
-----------------
To start the server use the next command from the application root dir:

    npm start

or

    node ./main.js

Yandex Music Integration
------------------------
To get  the currently playing music at the music.yandex player you should create a bookmarklet at the chrome browser:
````javascript
javascript:(() => {
	let curData = { artist: '', track: ''};
	setInterval(() => {
		let info = {artist:document.getElementsByClassName('d-artists d-artists__expanded')[0].children[0].title,track:document.getElementsByClassName('track__title')[0].attributes.title.nodeValue};
		if (curData.artist === info.artist && curData.track === info.track) {
			return;
		}
		let xhr = new XMLHttpRequest();  
        let host = '127.0.0.1:3030';        
		xhr.open("POST", 'http://' + host + '/music');
		xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');

		xhr.send(JSON.stringify(info));		
		
		xhr.onload = () => curData = info;
	}, 5000);
})();
````                                                                   
_**NOTICE:** Do not forget specify your server address at the JS code._ 

Use that bookmarklet once you started listen to the music. This script makes a post http request to the server once in a track.
 