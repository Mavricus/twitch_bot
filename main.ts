import { FsJsonConfigReader } from './src/configuration/corfig_reader';
import { AppServerFactory } from './src/server/server';
import { ServerRouterFactory } from './src/server/server_router';
import { IMusic, MusicHandlerFactory } from './src/server/routes/music_get';
import { ObjectStorage } from './src/utils/storages/object_storage';
import { InMemoryStorage } from './src/utils/storages/in_memory_storage';
import { FileStorage, FsStringStorage } from './src/utils/storages/fs_storage';
import { ProxyRouter } from './src/server/routes/proxy_router';
import { TwitchUsersApi } from './src/twitch_api/users_twitch_api';
import { TwitchApi } from './src/twitch_api/twitch_api';
import { TwitchChannelApi } from './src/twitch_api/chanel_twitch_api';
import { TwitchStreamApi } from './src/twitch_api/stream_twitch_api';
import { PeriodicMessageService, PredefinedSelfMessage } from './src/bot/services/periodic_messages_service';
import { ChatUserStorage } from './src/bot/users/chat_user_storage';
import { RudeWordsDto } from './src/data_sources/rode_words_dao';
import { DdConnection } from './src/data_sources/db_connection';
import { createPool } from 'mysql';
import { FollowersNumber } from './src/bot/services/followers_number';
import { UserDao } from './src/data_sources/users/user_dao';
import fs from 'fs';
import path from 'path';
import twitchClient from 'twitch-api-v5';
import * as optimist from 'optimist';
import { EventEmitter } from 'events';
import { UserDbStorage } from './src/bot/users/user_db_storage';
import { StreamDiscoveryService } from './src/bot/services/stream_discovery/stream_discovery_service';
import { StreamStatusControllerFactory } from './src/bot/services/stream_discovery/stream_status_controller';
import { StreamDto } from './src/data_sources/stream/stream_dto';
import { StreamersDao } from './src/data_sources/users/streamers_dao';
import { UserMessagesDao } from './src/data_sources/users/user_messages_dao';
import { AppBotFactory } from './src/bot/app_bot';
import { ConsoleLogger } from './src/utils/logger/console_logger';
import { ClientFactory } from './src/bot/client_factory';
import { DictionaryFactory } from './src/utils/collections';
import { TemporaryPenaltyStorageFactory } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/moderation/punishment/punishment_expiration_strategy';
import { BotCommandMessageScopeBuilder } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/commands/bot_command_mesage_scope_builder';
import { PunishmentCalculator } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/moderation/punishment/punishment_caclulator';
import { Punisher } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/moderation/punishment/punisher';
import { PunishmentExecutor } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/moderation/punishment/punisment_executor';
import { RudeWordsProvider } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/moderation/moderation_actions/rude_words/rude_words_reader';
import { ModerateRudeWords } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/moderation/moderation_actions/rude_words/moderate_rude_words';
import { ModerateCapsLock } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/moderation/moderation_actions/modarate_caps_lock';
import { Moderator } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/moderation/moderator';
import { BotCommandCollection } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/commands/bot_command_collection';
import { BotCommandMessageSenderWhisper } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/chat_message_senders/chat_message_sender_whisp';
import { BotCommandMessageSenderPub } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/chat_message_senders/chatmessage_sender_pub';
import {
    BotCommandHandlerFactory, IBotCommandHandler
} from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/commands/bot_commands_handler';
import { BotCommandMusicResponseBuilder } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/commands/message_builders/bot_command_music';
import { BotCommandSpotifyResponseBuilder } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/commands/message_builders/bot_commands_spotify';
import { BotCommandYandexMusicResponseBuilder } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/commands/message_builders/bot_commands_yandex_music';
import { BotCommandGameResponseBuilder } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/commands/message_builders/bot_commands_game';
import { BotCommandUptimeResponseBuilder } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/commands/message_builders/bot_command_uptime';
import {
    BotCommandTransliterateResponseBuilder, RuEngAlphabet
} from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/commands/message_builders/bot_command_transliterate';
import { BotCommandHelpResponseBuilder } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/commands/message_builders/bot_command_help';
import { BotCommandsPerformer } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/commands/bot_commands_performer';
import { GreetingHandler } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/greeting_handler';
import { MentioningHandler } from './src/bot/bot_event_processors/chat_message_event_processor/chat_message_handlers/mentioning_handler';
import { ChatListenersHandler } from './src/bot/bot_event_processors/chat_message_event_processor/chat_listeners_handler';
import {
    IUserMessageDetails, MessageInformationBuilder
} from './src/bot/bot_event_processors/chat_message_event_processor/message_information_builder';
import { UserStateBuilder } from './src/bot/bot_event_processors/user_state_builder';
import { ChatMessageProcessor } from './src/bot/bot_event_processors/chat_message_event_processor/bot_chat_message_event_processor';
import { BotSubscriptionEventProcessor } from './src/bot/bot_event_processors/subscription_event_processor/subscription_processor';
import { BotRaidedEventProcessor } from './src/bot/bot_event_processors/raid_event_processor/bot_raided_event_processor';
import { Events } from 'tmi.js';
import { BotConnectedEventProcessor } from './src/bot/bot_event_processors/connected_event_processor/bot_connected_event_processor';
import { MusicDao } from './src/data_sources/music_dao';
import SpotifyWebApi from 'spotify-web-api-node';
import { ISpotifyMusicDiscoveryConfig, SpotifyMusicDiscovery } from './src/bot/services/spotify_music_discovery';

const confPath = path.resolve(__dirname, optimist.argv.config || './cfg.json');
const configReader = new FsJsonConfigReader(confPath);
const config = configReader.get();

const logger = new ConsoleLogger(console).setLoglevel(config.logger.level);
logger.info({ description: 'About to start application.', requestId: config.systemRequestId, config });

namespace Twitch {
    twitchClient.clientID = config.bot.options.twitchApiClientId;
    const userApi = new TwitchUsersApi(twitchClient);
    const channelApi = new TwitchChannelApi(twitchClient);
    const streamApi = new TwitchStreamApi(twitchClient);
    export const api = new TwitchApi(userApi, channelApi, streamApi);

    const clientFactory = new ClientFactory(config);
    export const client = clientFactory.create();
}

namespace DataBase {
    const fileStorage = new FileStorage(path.resolve(__dirname, './db_emul.json'), fs);

    export namespace Moderation {
        const client = new DdConnection(createPool(config.db.moderation), logger);
        
        export namespace RudeWords {
            export const dto = new RudeWordsDto(client);
        }
    }

    export namespace User {
        const client = new DdConnection(createPool(config.db.users), logger);

        export const dao = new UserDao(client);

        export namespace Message {
            export const dao = new UserMessagesDao(client);
        }

        export namespace Streamer {
            export const dao = new StreamersDao(client, User.dao);
        }
    }

    export namespace Stream {
        export const dto = new StreamDto(fileStorage);
    }
}

namespace Storage {
    export const inMemory = new InMemoryStorage();
    export const fileStorage = new FileStorage(path.resolve(__dirname, './temp_storage.tmp'), fs);

    export namespace Object {
        export const touches = new ObjectStorage<number>(fileStorage, 'touches');
    }

    export namespace Music {
        const objStore = new ObjectStorage<IMusic>(fileStorage, 'music');
        const fsStore = new FsStringStorage(config.bot.options.musicFile, fs);
        export const dao = new MusicDao(objStore, fsStore);
    }

    export namespace Chat {
        const penaltyStorageFactory = new TemporaryPenaltyStorageFactory();
        const persistentDataStorage = new UserDbStorage(DataBase.User.dao);
        export const user = new ChatUserStorage(inMemory, persistentDataStorage, penaltyStorageFactory, logger);
    }
}

namespace Spotify {
    const client = new SpotifyWebApi({
        // clientId: config.spotify.clientId,
        // clientSecret: config.spotify.clientSecret,
        // redirectUri: config.spotify.redirectUri,
        accessToken: config.spotify.accessToken,
        refreshToken: config.spotify.refreshToken
    });
    
    const cfg: ISpotifyMusicDiscoveryConfig = {
        timeout: 1000,
        systemId: config.systemRequestId
    };

    export const discovery = new SpotifyMusicDiscovery(cfg, client, Storage.Music.dao, logger);
}

namespace Bot {
    const scopeBuilder = new BotCommandMessageScopeBuilder();
    const stateBuilder = new UserStateBuilder();

    export namespace Service {
        export namespace WaterWarn {
            let message = new PredefinedSelfMessage(config, 'полчаса прошло. Воды попей давай.');
            export const periodicMessages = new PeriodicMessageService(config, logger, Twitch.client, message);
        }

        export namespace Followers {
            export const watcher = new FollowersNumber(config, Twitch.api, fs);
        }

        export namespace StreamDiscovery {
            namespace Controller {
                let config = {
                    recoveryTimeout: 5 * 60 * 1000 // 5 minutes
                };
                export const factory = new StreamStatusControllerFactory(config);

            }
            const serviceConfig = {
                streamers: [config.bot.options.streamersName],
                streamCheckPeriod: 10_000 // Each 10 sec
            };
            const emitter = new EventEmitter();
            export const doer = new StreamDiscoveryService(serviceConfig, Controller.factory, Twitch.api, emitter, DataBase.User.Streamer.dao);
        }
    }

    export namespace Events {
        export namespace Chat {
            namespace Moderation {
                namespace Punishment {
                    const calculator = new PunishmentCalculator(logger);
                    const executor = new PunishmentExecutor(Twitch.client);
                    export const doer = new Punisher(calculator, executor, logger);
                }
                namespace RudeWords {
                    const dictionaryFactory = new DictionaryFactory(Storage.inMemory, 'rudeWords');
                    const provider = new RudeWordsProvider(dictionaryFactory, DataBase.Moderation.RudeWords.dto);
                    export const action = new ModerateRudeWords(provider);
                }
                namespace CapsLock {
                    export const action = new ModerateCapsLock();
                }

                export const handler = new Moderator(config, Punishment.doer, Twitch.client, logger);
                handler.registerModeratorAction(CapsLock.action, RudeWords.action);
            }

            namespace Commands {
                export const collection = new BotCommandCollection();
                namespace HandlerFactory {
                    const senderWhisper = new BotCommandMessageSenderWhisper(Twitch.client);
                    const senderPub = new BotCommandMessageSenderPub(Twitch.client);

                    export const say = new BotCommandHandlerFactory(scopeBuilder, senderPub, logger);
                    export const whisper = new BotCommandHandlerFactory(scopeBuilder, senderWhisper, logger);
                }

                // namespace Virus {
                //     const cfg: IBotCommandCounterConfig = {
                //         streamerName: config.bot.options.streamersName,
                //         description: 'Считает сколько за стрим раз я потрогал свое лицо',
                //         messageTemplate: 'хватит трогать себя в разных местах. {sender} подтвердит, что уже {count} раз потрогал.'
                //     }
                //     export const triggers = ['!нетрож'];
                //     const builder = new BotCommandCounterResponseBuilder(cfg, Storage.Object.touches);
                //     export const command = HandlerFactory.say.create(builder);
                // }


                namespace Music {
                    export const triggers = ['!!m', '!music', '!музыка', '!song', '!песня'];
                    const builder = new BotCommandMusicResponseBuilder(config, Storage.Music.dao, Twitch.api);
                    export const command = HandlerFactory.say.create(builder);
                }

                namespace Spotify {
                    export const triggers = ['!!s', '!spotify'];
                    const builder = new BotCommandSpotifyResponseBuilder(Storage.Music.dao);
                    export const command = HandlerFactory.say.create(builder);
                }

                namespace YandexMusic {
                    export const triggers = ['!!y', '!ymusic', '!ямузик', '!ямузыка'];
                    const builder = new BotCommandYandexMusicResponseBuilder(Storage.Music.dao);
                    export const command = HandlerFactory.say.create(builder);
                }

                namespace Game {
                    export const triggers = ['!!g', '!game', '!игра'];
                    const builder = new BotCommandGameResponseBuilder(config, Twitch.api);
                    export const command = HandlerFactory.say.create(builder);
                }

                namespace Uptime {
                    export const triggers = ['!!t', '!t', '!up', '!uptime', '!time', '!online'];
                    const builder = new BotCommandUptimeResponseBuilder(config, Twitch.api);
                    export const command = HandlerFactory.say.create(builder);
                }

                namespace Transliterate {
                    export const triggers = ['!переведи', '!эльфийский', '!\'kmabqcrbq'];
                    const builder = new BotCommandTransliterateResponseBuilder(RuEngAlphabet);
                    export const command = HandlerFactory.say.create(builder);
                }

                namespace Help {
                    export const triggers = ['!!h', '!help', '!помогите', '!команды', '!нипанятно', '!commands'];
                    const builder = new BotCommandHelpResponseBuilder(collection);
                    export const command = HandlerFactory.say.create(builder);
                }

                const map: Array<{ triggers: Array<string>, command: IBotCommandHandler }> = [
                    Music, Spotify, YandexMusic, Game, Uptime, Transliterate, /*Virus,*/ Help
                ];

                map.forEach(info => collection.registerCommand(info.triggers, info.command));

                export const handler = new BotCommandsPerformer(collection, logger);
            }

            export namespace Greeting {
                export const handler = new GreetingHandler(config, Twitch.client, scopeBuilder);
            }

            namespace Mentioning {
                export const handler = new MentioningHandler(config, Twitch.client);
            }

            const messageBuilder = new MessageInformationBuilder(Storage.Chat.user, DataBase.User.Streamer.dao, DataBase.User.Message.dao);
            const handler = new ChatListenersHandler();
            handler.addMessageListener((requestId: string, message: IUserMessageDetails) => Commands.handler.handle(requestId, message));
            handler.addMessageListener((requestId: string, message: IUserMessageDetails) => Moderation.handler.handle(requestId, message));
            handler.addMessageListener((requestId: string, message: IUserMessageDetails) => Greeting.handler.handle(requestId, message));
            handler.addMessageListener((requestId: string, message: IUserMessageDetails) => Mentioning.handler.handle(requestId, message));
            
            export const processor = new ChatMessageProcessor(config, messageBuilder, handler, stateBuilder, logger);
        }

        namespace Connected {
            export const processor = new BotConnectedEventProcessor({ channel: config.bot.config.channels?.[0] ?? '' }, Twitch.client);
        }

        namespace Subscription {
            export const processor = new BotSubscriptionEventProcessor(Twitch.client, stateBuilder, logger);
        }

        namespace Raid {
            export const processor = new BotRaidedEventProcessor(config, Twitch.client, logger);
        }

        export const processorsMap: Partial<Events> = {
            connected: (address: string, port: number) => Connected.processor.connected(address, port),
            chat: (channel, userState, message, self) => Chat.processor.chat(channel, userState, message, self),
            subscription: (channel, username, methods, message, userState) =>
                Subscription.processor.process(channel, username, methods, message, userState),
            raided: (channel, username, viewers) => Raid.processor.raided(channel, username, viewers)
        }

    }
    export const app = new AppBotFactory(Events.processorsMap);
}

namespace Server {
    const proxy = new ProxyRouter(logger);
    const musicFactory = new MusicHandlerFactory(Storage.Music.dao, logger);
    const routerFactory = new ServerRouterFactory(musicFactory, proxy);
    export const app = new AppServerFactory(routerFactory);
}

// const app = new Application(config, logger);

process.nextTick(() => {
    // app.start();

    let followersInterval: NodeJS.Timer | null = null;

    Bot.Service.StreamDiscovery.doer.on('online', () => {
        Bot.Service.WaterWarn.periodicMessages.start(30 * 60);
        Twitch.client.say(config.bot.config.channels?.[0] || config.bot.options.streamersName, 'Потекло!');
        if (followersInterval == null) {
            followersInterval = setInterval(() => Bot.Service.Followers.watcher.updateFollowersNumber(), 5000);
        }

        Bot.Events.Chat.Greeting.handler.start();
        Spotify.discovery.start();
    });

    Bot.Service.StreamDiscovery.doer.on('offline', () => {
        Bot.Service.WaterWarn.periodicMessages.stop();
        Twitch.client.say(config.bot.config.channels?.[0] || config.bot.options.streamersName, 'Бабай!');
        Storage.Object.touches.set(0);
        Bot.Events.Chat.Greeting.handler.stop();
        Spotify.discovery.stop();

        if (followersInterval != null) {
            clearInterval(followersInterval);
        }
    });

    Bot.Service.StreamDiscovery.doer.on('recover', () => {
        Twitch.client.say(config.bot.config.channels?.[0] || config.bot.options.streamersName, 'Он вернулся! Вернууууусяяяя!');
    });

    Bot.Service.StreamDiscovery.doer.start();
    Bot.app.start(Twitch.client);

    Promise.all([Twitch.client.connect(), Server.app.startServer(config)])
           .then(() => logger.info({ description: 'Server and bot are started.', requestId: config.systemRequestId }))
           .catch((err) => logger.emergency({ description: 'Cannot start server and/or bot.', requestId: config.systemRequestId, err }));
});
